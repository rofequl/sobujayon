@extends('frontend.layout.app')
@section('content')


    <div class="page-title grey">
        <div class="container">
            <div class="title-area text-center">
                <h2>Shopping</h2>
                <div class="bread">
                    <ol class="breadcrumb">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li class="active">Gift</li>
                    </ol>
                </div><!-- end bread -->
            </div><!-- /.pull-right -->
        </div>
    </div><!-- end page-title -->


    <section class="section white">
        <div class="container">
            <div class="shop-top row clearfix">
                <div class="title-area pull-left">
                    <p>Showing {{($product->currentpage()-1)*$product->perpage()+1}}–
                        {{$product->currentpage()*$product->perpage()}} of {{$product->total()}} results</p>
                </div><!-- /.pull-right -->
                <div class="pull-right">
                    <select class="selectpicker" data-style="btn-primary" id="selectTeam" onchange="refreshOrg()">
                        <option value="default">Default sorting</option>
                        <option value="new">Sort by newness</option>
                        <option value="low">Sort by price: low to high</option>
                        <option value="high">Sort by price: high to low</option>
                    </select>
                </div><!-- /.pull-right -->
            </div><!-- end shop top -->

            <script>
                function refreshOrg() {
                    window.location='{{route('gift')}}?type='+$('#selectTeam').val();
                }
            </script>

            <div class="row">
                <div id="content" class="col-md-12 col-sm-12 col-xs-12">
                    <div class="blog-wrapper">
                        <div class="row module-wrapper shop-layout text-center">

                            @foreach($product as $products)

                                <div class="col-md-3 col-sm-3 shop-item">
                                    <div class="entry">
                                        <img class="img-responsive" src="{{asset('storage/tree/'.$products->image)}}" style="height: 180px;width: 100%" alt="">
                                        <div class="magnifier">
                                        </div><!-- end magnifier -->
                                    </div><!-- end entry -->
                                    <div class="product-body text-left">
                                        <h2 class="product-name"><a href="{{route('SingleShop','product='.$products->id)}}" class="c-product-name">{{$products->name->name}}</a></h2>
                                        <h3 class="product-price c-product-price">&#2547;{{$products->price - $products->discount}}
                                            <del class="product-old-price">&#2547;{{$products->price}}</del>
                                        </h3>
                                        <div class="product-btns">
                                            <!--<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>-->
                                            <!--<button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>-->
                                            <button onclick="window.location.href='{{route('SingleShop','product='.$products->id)}}'"
                                                    class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add
                                                to Cart
                                            </button>
                                        </div>
                                    </div>
                                </div>

                            @endforeach


                        </div><!-- end row -->
                    </div><!-- end blog-wrapper -->

                    <hr class="invis">

                    <nav class="pagi clearfix text-center">
                        {{ $product->links() }}
                    </nav>
                </div><!-- end content -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->



@endsection
