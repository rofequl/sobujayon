@extends('frontend.layout.app')
@section('content')


    <div class="page-title grey">
        <div class="container">
            <div class="title-area text-center">
                <h2>Single Shop</h2>
                <div class="bread">
                    <ol class="breadcrumb">
                        <li><a href="#">Home</a></li>
                        <li class="active">Shopping Cart</li>
                    </ol>
                </div><!-- end bread -->
            </div><!-- /.pull-right -->
        </div>
    </div><!-- end page-title -->

    <section class="section white">
        <div class="container">
            <div class="row">
                <div id="content" class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                    @if(session()->has('message'))
                        <div class="alert alert-warning alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('message') }}
                        </div>
                    @endif

                        <div class="table-responsive margin-top">
                            <table id="cart-table" class="table table-condensed">
                                <thead>
                                <tr>
                                    <th>Action</th>

                                    <th>Image</th>
                                    <th>Product</th>
                                    <th>Price</th>
                                    <th>Quanity</th>
                                    <th>Lift</th>
                                    <th>Delivery Cost</th>
                                    <th>Lift Cost</th>
                                    <th>Total</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $discount = 0; @endphp
                                @foreach(Cart::content() as $row)
                                    @php $discount += (get_product_by_id($row->id)->discount * $row->qty); @endphp
                                    <tr>
                                        <th class="product-remove">
                                            <a class="remove" title="Remove this product"
                                               href="{{route('ShopCartRemove','remove='.$row->rowId)}}">×</a>
                                        </th>
                                        <th>
                                            <div class="media">
                                                <div class="relative">
                                                    <a href="{{route('SingleShop','product='.$row->id)}}" title="">
                                                        <img
                                                            src="{{asset('storage/tree/'.get_product_by_id($row->id)->image)}}"
                                                            alt="">
                                                    </a>
                                                </div>
                                            </div><!-- end media -->
                                        </th>
                                        <th>
                                            <a href="{{route('SingleShop','product='.$row->id)}}">{{$row->name}}</a>
                                        </th>
                                        <td>
                                            &#2547;{{get_product_by_id($row->id)->price}}</td>
                                        <td>
                                            <form class="form-inline" name="CartUpdate{{$row->id}}" id="CartUpdate{{$row->id}}" action="{{route('CartUpdate')}}" method="post">
                                                @csrf
                                                <div class="form-group">
                                                    <label class="sr-only" for="exampleInputAmount">Amount (in dollars)</label>
                                                    <div class="input-group input-group-sm">
                                                        <input type="hidden" name="rowId" value="{{$row->rowId}}">
                                                        <input type="number" min="1" name="qty" value="{{$row->qty}}" class="form-control input-sm" id="exampleInputAmount" placeholder="Qty">
                                                        <div onclick="document.forms['CartUpdate{{$row->id}}'].submit();"
                                                             class="input-group-addon  btn-primary"><i class="fa fa-repeat" aria-hidden="true"></i></div>
                                                    </div>
                                                </div>
                                            </form>
                                        </td>
                                        <td>
                                            {{$row->options->lift}}
                                        </td>
                                        <td>
                                            &#2547;{{$row->options->delivery_cost}}
                                        </td>
                                        <td>
                                            &#2547;{{$row->options->lift_cost}}
                                        </td>
                                        <td>
                                            &#2547;{{$row->price + (get_product_by_id($row->id)->discount * $row->qty)}}
                                        </td>
                                    </tr>
                                @endforeach
                                @if(Cart::content()->count() == 0)
                                    <tr>
                                        <td colspan="9">
                                            <h4 class="text-center">You can't add to cart any product</h4>
                                        </td>
                                    </tr>
                                @else
                                    <tr>
                                        <td colspan="8" style="padding: 0">
                                            <h4 class="text-right">Subtotal:</h4>
                                        </td>
                                        <td style="padding: 0">
                                            @php $total=0; @endphp
                                            @foreach(Cart::content() as $row) @php $total=$row->price+$total; @endphp @endforeach
                                            <h4 class="text-center">&#2547;{{$total + $discount}}</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="border: 0;padding: 0">
                                            <h4 class="text-right">Discount:</h4>
                                        </td>
                                        <td style="border: 0;padding: 0">
                                            <h4 class="text-center">&#2547;{{$discount}}</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="8" style="border: 0;padding: 0">
                                            <h4 class="text-right">Total:</h4>
                                        </td>
                                        <td style="border: 0;padding: 0">
                                            @php $total=0; @endphp
                                            @foreach(Cart::content() as $row) @php $total=$row->price+$total; @endphp @endforeach
                                            <h4 class="text-center">&#2547;{{$total}}</h4>
                                        </td>
                                    </tr>
                                @endif
                                </tbody>
                            </table>
                        </div><!-- end table -->

                    <hr class="invis">

                    <div class="checkout row">
                        <div class="col-md-12 text-center">
                            <a href="{{route('shop')}}" class="btn btn-primary">CONTINUE SHOP</a>
                            @if(Cart::content()->count() != 0)
                                <a href="{{route('checkout')}}" class="btn btn-success"> PROCEED TO CHECKOUT</a>
                            @endif
                        </div>
                    </div><!-- end checkout -->
                </div><!-- end content -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->



@endsection
@push('style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/prettyPhoto.css')}}">
@endpush
@push('script')
    <script src="{{asset('assets/frontend/js/jquery.prettyPhoto.js')}}"></script>
@endpush
