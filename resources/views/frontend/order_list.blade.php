@extends('frontend.layout.app')
@section('content')



    <div class="wrapper">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <br>
                    <div class="card m-b-20">
                        <h1 style="font-size:4em;color: #1BBD36 ;shape-rendering:5px; text-align: center;"> Client Order
                            List</h1>
                        <hr>
                        @if(session()->has('message'))
                            <div class="alert alert-success alert-dismissible">
                                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                {{ session()->get('message') }}
                            </div>
                        @endif
                        <div class="card-body">
                            <div class="table-responsive">
                                <table style="text-align: center;" id="datatable-buttons"
                                       class="table table-striped table-bordered" cellspacing="0" width="100%">
                                    <thead>
                                    <tr>
                                        <th>SL</th>
                                        <th>Order ID</th>
                                        <th>Product Name</th>
                                        <th>Price</th>
                                        <th>Discount</th>
                                        <th>Qty</th>
                                        <th>Lift</th>
                                        <th>Delivery cost</th>
                                        <th>Lift cost</th>
                                        <th>Total cost</th>
                                        <th>Photo</th>
                                        <th>Status</th>
                                        <th> Action</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $sl = 1; @endphp
                                    @foreach($order as $orders)
                                        @foreach(get_order_product_by_order_id($orders->id) as $product)
                                            <tr>
                                                <td>{{$sl}}</td>@php $sl++; @endphp
                                                <td>{{$orders->order_id}}</td>
                                                <td>{{$product->name}}</td>
                                                <td>&#2547;{{$product->product->price}}</td>
                                                <td>&#2547;{{$product->product->discount}}</td>
                                                <td>{{$product->qty}}</td>
                                                <td>{{$product->lift}}</td>
                                                <td>&#2547;{{$product->delivery_cost}}</td>
                                                <td>&#2547;{{$product->lift_cost}}</td>
                                                <td>&#2547;{{$product->total_price}}</td>
                                                <td><img class="rounded-circle" alt="200x200"
                                                         src="{{asset('storage/tree/'.get_product_by_id($product->product_id)->image)}}"
                                                         width="80px" height="80px" data-holder-rendered="true">
                                                </td>
                                                <td>
                                                    @if($orders->status == 1)
                                                        <span
                                                            class="label label-green">Order Confirmed</span>
                                                    @elseif($orders->status == 2)
                                                        <span
                                                            class="label label-mint">Picked and Packed</span>
                                                    @elseif($orders->status == 3)
                                                        <span class="label label-success">Shipped</span>
                                                    @elseif($orders->status == 4)
                                                        <span class="label label-primary">Delivered</span>
                                                    @elseif($orders->status == 5)
                                                        <span class="label label-danger">Rejected</span>
                                                    @else
                                                        <span class="label label-lime p-1">Order Request</span>
                                                    @endif
                                                    @if($orders->payment == 1)
                                                            <br><br><span class="label label-magneta p-1">Paid</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($orders->status == 0 && $orders->payment == 0)
                                                        <button type="button"
                                                                onclick="window.location.href='{{route('OrderListCancel',$product->id)}}'"
                                                                class="btn btn-outline-warning waves-effect waves-light">
                                                            order
                                                            Cancel
                                                        </button>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                                <nav class="pagi clearfix text-center">
                                    {{ $order->links() }}
                                </nav>
                            </div>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->

        </div> <!-- end container -->
    </div>
    <!-- end wrapper -->
    <br>



@endsection
@push('style')
    <link rel="stylesheet" type="text/css" href="{{asset('assets/frontend/css/prettyPhoto.css')}}">
@endpush
@push('script')
    <script src="{{asset('assets/frontend/js/jquery.prettyPhoto.js')}}"></script>
@endpush
