@extends('frontend.layout.app')
@section('content')


    <div class="page-title grey">
        <div class="container">
            <div class="title-area text-center">
                <h2>Single Shop</h2>
                <div class="bread">
                    <ol class="breadcrumb">
                        <li><a href="{{route('home')}}">Home</a></li>
                        <li class="active">Checkout</li>
                    </ol>
                </div><!-- end bread -->
            </div><!-- /.pull-right -->
        </div>
    </div><!-- end page-title -->


    <section class="section white">
        <div class="container">
            <div class="row">
                <div id="content" class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">

                    <form method="post" class="shopform" action="{{route('CheckoutStore')}}" id="checkout-form">
                        @csrf
                        <div class="row">
                            <div class="col-md-12">
                                @if ($errors->any())
                                    @foreach ($errors->all() as $error)
                                        <div class="alert alert-warning alert-dismissible">
                                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                            {{$error}}
                                        </div>
                                    @endforeach
                                @endif
                                @if(session()->has('message'))
                                    <div class="alert alert-success alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                        {{ session()->get('message') }}
                                    </div>
                                @endif
                                <div class="widget-title">
                                    <h1>write your address</h1>
                                </div>
                                <div class="row">
                                    <div class="form-group row"
                                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                        <textarea id="textarea" name="address" class="form-control" maxlength="1000" rows="3"
                                                  placeholder="Write Your Details Address"></textarea>
                                        </select>
                                    </div>
                                </div>
                            </div><!-- end row -->
                        </div><!-- end col -->
                </div><!-- end row -->
                </form>
                <hr class="invis">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 text-center">
                    <button type="submit" value="SEND" id="submit"
                            onclick="document.getElementById('checkout-form').submit();"
                            class="btn btn-primary btn-block btn-lg">Place In
                        Order
                    </button>
                </div>
                </form>
            </div><!-- end totalarea -->
        </div><!-- end content -->
        </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->


@endsection
@push('style')

@endpush
@push('script')

@endpush
