@extends('frontend.layout.app')
@section('content')


    <div class="page-title grey">
        <div class="container">
            <div class="title-area text-center">
                <h2>Single Shop</h2>
                <div class="bread">
                    <ol class="breadcrumb">
                        <li><a href="#">{{$product->category->category}}</a></li>
                        <li><a href="#">{{$product->subcategory->subcategory}}</a></li>
                        <li>
                            @if($product->brand_id)
                                @foreach(explode(',',$product->brand_id) as $brand)
                                    <a href="{{route('shop','brand='.get_brand_by_id($brand)->id)}}">{{get_brand_by_id($brand)->name}}</a>
                                    /
                                @endforeach
                            @endif
                        </li>
                    </ol>
                </div><!-- end bread -->
            </div><!-- /.pull-right -->
        </div>
    </div><!-- end page-title -->


    <section class="section white">
        <div class="container">
            <div class="row">
                <div id="content" class="col-md-10 col-md-offset-1 col-sm-12 col-xs-12">
                    <div class="row">
                        <div class="col-md-5 col-sm-5 col-xs-12">
                            <!--CART STRAT-->
                            <div class="exzoom" id="exzoom">
                                <!-- Images -->
                                <div class="exzoom_img_box">
                                    <ul class='exzoom_img_ul'>
                                        <li><img src="{{asset('storage/tree/'.$product->image)}}"/></li>
                                        <li><img src="{{asset('storage/tree/'.$product->image2)}}"/></li>
                                        <li><img src="{{asset('storage/tree/'.$product->image3)}}"/></li>
                                        <li><img src="{{asset('storage/tree/'.$product->image4)}}"/></li>
                                        ...
                                    </ul>
                                </div>
                                <!-- <a href="https://www.jqueryscript.net/tags.php?/Thumbnail/">Thumbnail</a> Nav-->
                                <div class="exzoom_nav"></div>
                                <!-- Nav Buttons -->
                                <p class="exzoom_btn">
                                    <a href="javascript:void(0);" class="exzoom_prev_btn"> < </a>
                                    <a href="javascript:void(0);" class="exzoom_next_btn"> > </a>
                                </p>
                            </div>
                            <!--CART END-->
                        </div>
                        <div class="col-md-7 col-sm-7 col-xs-12">
                            <div class="shop-desc">
                                <h3>{{$product->name->name}}</h3>
                                <small>&#2547;{{$product->price - $product->discount}}</small>

                                <h4 style="font-family:'Galada' "> Color: {{$product->color}} | {{$product->qty}}
                                    product
                                    has</h4>

                                <p style="padding-bottom: 4px">Item weight: {{$product->color}}</p>
                                <p style="padding-bottom: 4px">Price: {{$product->price}}</p>
                                <p style="padding-bottom: 4px">Discount: {{$product->discount}}</p>
                                <p style="padding-bottom: 4px">Height & Width: {{$product->height->height}}
                                    & {{$product->width->width}}</p>
                                <p style="padding-bottom: 4px">Origin Country: {{$product->origin_country->country}}</p>

                                <a style="font-family:'Galada';font-size: large " href=""><img
                                        src="{{asset('assets/frontend/upload/35.png')}}">#1
                                    Best Seller</img><span>in </span><span
                                        class="cat-link"> Outdoor Recreation nursery</span></a>
                                <div class="clearfix">
                                    <div class="row" style="margin-top: 10px">
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <span class="input-group-addon btn btn-success plus"><i
                                                        class="fa fa-plus"></i></span>
                                                <input id="msg" type="number" class="form-control seat AllEdit"
                                                       name="msg"
                                                       placeholder="Product Quantity">
                                                <span class="input-group-addon btn btn-success minus"><i
                                                        class="fa fa-minus"></i></span>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="input-group">
                                                <span class="input-group-addon btn btn-success plus1"><i
                                                        class="fa fa-plus"></i></span>
                                                <input id="msg" type="number" class="form-control seat1 AllEdit"
                                                       name="msg"
                                                       placeholder="Building Floor">
                                                <span class="input-group-addon btn btn-success minus1"><i
                                                        class="fa fa-minus"></i></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-6">
                                        <a href="#" class="btn btn-primary add-cart" id="{{$product->id}}">Add to
                                            Cart</a>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="row" style="margin-top: 30px">
                                            <div class="col-md-6 text-right" style="line-height: 43px;">
                                                Delivery Cost:
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" id="delivery_cost"
                                                       class="form-control form-control-sm" value=""
                                                       readonly="">
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px">
                                            <div class="col-md-6 text-right" style="line-height: 43px;">
                                                Lift Cost:
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" id="lift_cost" class="form-control form-control-sm"
                                                       value=""
                                                       readonly="">
                                            </div>
                                        </div>
                                        <div class="row" style="margin-top: 10px">
                                            <div class="col-md-6 text-right" style="line-height: 43px;">
                                                Total Cost:
                                            </div>
                                            <div class="col-md-6">
                                                <input type="text" id="total_cost" class="form-control" value=""
                                                       readonly="">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- end shopmeta -->

                            </div><!-- end desc -->
                        </div><!-- end col -->
                    </div><!-- end row -->

                    <hr class="invis">

                    <div class="row">
                        <div class="col-md-12">
                            <div class="service-style-1">
                                <div class="tabbed-widget">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#home">Descriptions</a></li>
                                        <li style="width: 200px;"><a data-toggle="tab" href="#menu1">Product Details</a>
                                        <li style="width: 220px;"><a data-toggle="tab" href="#menu2">What you will
                                                get?</a>
                                        <li style="width: 200px;"><a data-toggle="tab" href="#menu3">product Video?</a>
                                        <li style="width: 150px;"><a data-toggle="tab" href="#menu4">Shipping</a>
                                        </li>
                                    </ul>

                                    <div class="tab-content">
                                        <div id="home" class="tab-pane fade in active">
                                            <p><img src="{{asset('storage/tree/convert/'.$product->image)}}"
                                                    class="alignleft" alt="">
                                                {{$product->short_description}} </p>
                                            <br>
                                            <p>{{$product->long_description}}</p>
                                        </div>
                                        <div id="menu1" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-md-6">
                                                    <p style="padding-bottom: 0">
                                                        Category: {{$product->category->category}}</p>
                                                    <p style="padding-bottom: 0">
                                                        Subcategory: {{$product->subcategory->subcategory}}</p>
                                                    <p style="padding-bottom: 0">
                                                        Color: {{$product->color}}</p>
                                                    <p style="padding-bottom: 0">
                                                        Age: {{$product->age->age}}</p>
                                                    <p style="padding-bottom: 0">
                                                        With fruit: {{$product->with_fruit}}</p>
                                                    <p style="padding-bottom: 0">
                                                        With flower: {{$product->with_flower}}</p>
                                                </div>
                                                <div class="col-md-6">
                                                    <p style="padding-bottom: 0">Origin
                                                        Country: {{$product->origin_country->country}}</p>
                                                    <p style="padding-bottom: 0">
                                                        Temperature: {{$product->temperature->temperature}}</p>
                                                    <p style="padding-bottom: 0">
                                                        Height: {{$product->height->height}}</p>
                                                    <p style="padding-bottom: 0">
                                                        Width: {{$product->width->width}}</p>
                                                    <p style="padding-bottom: 0">
                                                        Gift: {{$product->gift}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="menu2" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p style="padding-bottom: 0">
                                                        {{$product->what_you_will_get}}</p>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="menu3" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    @php
                                                        $url=$product->video;

                                                        parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
                                                    @endphp
                                                    <iframe width="560" height="315"
                                                            src="https://www.youtube.com/embed/{{isset($my_array_of_vars['v'])?$my_array_of_vars['v']:''}}"
                                                            frameborder="0" gesture="media"
                                                            allow="autoplay; encrypted-media" allowfullscreen></iframe>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="menu4" class="tab-pane fade">
                                            <div class="row">
                                                <div class="col-md-12">
                                                    <p style="padding-bottom: 0">
                                                    Modify live shipping rates: surcharge, deduct or override rates,
                                                    provided by carriers. E.g. surcharge rates for delivery to Hawaii
                                                    and Alaska, correct rates for LTL freights.</p>

                                                    <ul style="list-style: circle;padding: 0 15px">
                                                        <li>Shipping rules based on order, product and customer
                                                            attributes
                                                        </li>
                                                        <li>Modify rates both for the whole cart and a single product
                                                        </li>
                                                        <li>Set up shipping based on customer groups and store views
                                                        </li>
                                                        <li>Change delivery prices both by flat amount and by percent
                                                        </li>
                                                        <li>Provide correct rates for LTL freights</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div><!-- end tabbed-widget -->
                            </div><!-- end service-style-1 -->
                        </div><!-- end col -->
                    </div><!-- end row -->

                    <hr class="invis">

                    @if($recent_product->count()>2)
                        <div class="general-title text-left">
                            <h4>Recently Viewed Product</h4>
                            <p class="lead">Listed below your recent view items!</p>
                        </div><!-- end general title -->
                        <div id="owl-sticky" class="row module-wrapper shop-layout text-center owl-sticky sticky-row">
                            @foreach($recent_product as $recent_products)
                                <div class="col-3 sticky-col">
                                    <div class="imageWrapper">
                                        <img class="img-responsive"
                                             src="{{asset('storage/tree/'.$recent_products->image)}}" alt="">
                                    </div><!-- end entry -->
                                    <div class="product-body">
                                        <h4 style="color: green" class="product-price">
                                            &#2547;{{$recent_products->price - $recent_products->discount}}
                                            <del class="product-old-price">&#2547;{{$recent_products->price}}</del>
                                        </h4>
                                        <h2 class="product-name"><a href="#">{{$recent_products->name->name}}</a></h2>
                                        <div class="product-btns">
                                            <!--<button class="main-btn icon-btn"><i class="fa fa-heart"></i></button>-->
                                            <!--<button class="main-btn icon-btn"><i class="fa fa-exchange"></i></button>-->
                                            <button
                                                onclick="window.location.href='{{route('SingleShop','product='.$recent_products->id)}}'"
                                                class="primary-btn add-to-cart"><i class="fa fa-shopping-cart"></i> Add
                                                to Cart
                                            </button>
                                        </div>
                                    </div>
                                </div><!-- end shop_item -->
                            @endforeach
                        </div><!-- end row -->
                    @endif


                </div><!-- end content -->
            </div><!-- end row -->
        </div><!-- end container -->
    </section><!-- end section -->


@endsection
@push('style')

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!--single shop -->


    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="{{asset('assets/frontend/js/jquery.exzoom.js')}}"></script>
    <link href="{{asset('assets/frontend/css/jquery.exzoom.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css"/>
@endpush
@push('script')
    <script src="{{asset('assets/frontend/js/jquery.exzoom.js')}}"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>

    <script src="{{asset('assets/frontend/js/jquery.prettyPhoto.js')}}"></script>
    <script type="text/javascript">
        (function ($) {
            "use strict";
            jQuery('a[data-gal]').each(function () {
                jQuery(this).attr('rel', jQuery(this).data('gal'));
            });
            jQuery("a[data-rel^='prettyPhoto']").prettyPhoto({
                animationSpeed: 'slow',
                theme: 'light_square',
                slideshow: true,
                overlay_gallery: true,
                social_tools: false,
                deeplinking: false
            });
        })(jQuery);
    </script>
    <!--cart-->
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        $(function () {
            $("#exzoom").exzoom({
                "navWidth": 72,
                "navHeight": 72,
                "navItemNum": 4,
                "navItemMargin": 10,
                "navBorder": 1,
                "autoPlay": false,
                "autoPlayTimeout": 2000
            });
        });
        $(document).ready(function () {
            $(function () {
                $(".plus").click(function (e) {
                    e.preventDefault();
                    var $this = $(this);
                    var $input = $(".seat");
                    var value = parseInt($input.val());
                    if (value < {{$product->qty}}) {
                        value = value + 1;
                    } else {
                        value = 1;
                    }
                    $input.val(value).trigger('change');
                });
                $(".minus").click(function (e) {
                    e.preventDefault();
                    var $this = $(this);
                    var $input = $(".seat");
                    var value = parseInt($input.val());
                    if (value > 1) {
                        value = value - 1;
                    } else {
                        value = 1;
                    }
                    $input.val(value).trigger('change');
                });
            });

            $(function () {
                $(".plus1").click(function (e) {
                    e.preventDefault();
                    var $this = $(this);
                    var $input = $(".seat1");
                    var value = parseInt($input.val());
                    if (value < 100) {
                        value = value + 1;
                    } else {
                        value = 1;
                    }
                    $input.val(value).trigger('change');
                });
                $(".minus1").click(function (e) {
                    e.preventDefault();
                    var $this = $(this);
                    var $input = $(".seat1");
                    var value = parseInt($input.val());
                    if (value > 1) {
                        value = value - 1;
                    } else {
                        value = 0;
                    }
                    $input.val(value).trigger('change');
                });
            });

            $(document).on('click', '.add-cart', function () {
                event.preventDefault();
                let id = $(this).attr('id');
                let lift = parseInt($(".seat1").val());
                var $input = $(".seat");
                var value = parseInt($input.val());
                if(isNaN(lift) || isNaN(value) || lift <= 0 ||value <= 0){
                    swal({
                        title: "",
                        text: "Please enter product quantity and building floor.",
                        type: "warning",
                    });
                    return;
                }
                $.ajax({
                    url: '{{route('AddCart')}}',
                    type: 'post',
                    data: {_token: CSRF_TOKEN, id: id, qty: value, lift: lift},
                    success: function (response) {
                        if (response == 1) {
                            swal({
                                title: "",
                                text: "Item is already in your cart",
                                type: "warning",
                            });
                        } else if (response == 3) {
                            swal({
                                title: "",
                                text: "Item has been moved to cart",
                                type: "success",
                            });
                            if ($('.CartCount').hasClass('hidden')) {
                                $('.CartCount').removeClass('hidden');
                                $('.CartCount').html(parseInt($('.CartCount').text()) + 1);
                            } else {
                                $('.CartCount').html(parseInt($('.CartCount').text()) + 1);
                            }
                        } else {
                            swal({
                                title: "Error",
                                text: "Something wrong, please try again later",
                                type: "warning",
                            });
                        }
                    }
                });

            });
            total_cost();

            function total_cost() {
                let quantity = parseInt($(".seat").val());
                let lift = parseInt($(".seat1").val());
                let id = parseInt('{{$product->id}}');
                $.ajax({
                    url: '{{route('TotalCost')}}',
                    type: 'post',
                    dataType: 'json',
                    data: {_token: CSRF_TOKEN, id: id, lift: lift, qty: quantity},
                    success: function (response) {
                        $('#delivery_cost').val(response.delivery_cost);
                        $('#lift_cost').val(response.lift_cost);
                        $('#total_cost').val(response.total_cost);
                    }
                });
            }

            $('.AllEdit').on('change paste keyup', function () {
                total_cost();
            });
        });

    </script>
@endpush
