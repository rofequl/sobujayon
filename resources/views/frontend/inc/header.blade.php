<style>
    .linktext {
        cursor: pointer;
        color: rgb(102, 102, 102) !important;
        margin: 0 0 2px !important;
        padding: 0;
        font-size: 12px !important;
        line-height: 12px;
    }

    .linktext:hover {
        color: rgb(254, 86, 33) !important;
    }
</style>

<div class="topbar clearfix">
    <div class="container">
        <div class="row-fluid">
            <div class="col-md-6 text-left">
                <div class="social">
                    <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Facebook"><i
                            class="fa fa-facebook"></i></a>
                    <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Twitter"><i
                            class="fa fa-twitter"></i></a>
                    <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Google Plus"><i
                            class="fa fa-google-plus"></i></a>
                    <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Linkedin"><i
                            class="fa fa-linkedin"></i></a>
                    <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Youtube"><i
                            class="fa fa-youtube"></i></a>
                    <a href="#" data-tooltip="tooltip" data-placement="bottom" title="Pinterest"><i
                            class="fa fa-pinterest"></i></a>
                </div><!-- end social -->
            </div><!-- end left -->
            <div class="col-md-6 text-right">
                <p>
                    <strong style="color: white"><i class="fa fa-phone"></i></strong> +90 543 123 45 67 &nbsp;&nbsp;
                    <strong style="color: white"><i class="fa fa-envelope"></i></strong> <a
                        href="mailto:info@yoursite.com">info@yoursite.com</a>
                </p>
            </div><!-- end left -->
        </div><!-- end row -->
    </div><!-- end container -->
</div>
<div id="wrapper" class="border sticky-top"
     style="position: -webkit-sticky; position: sticky;top: 0;z-index: 1020;">
    <div class="container"
         style=" padding:0.4em 0.3em;background:#eaeaea; text-align:center; width: 100%">
        <div class="row" style="margin-right: 0">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 text-left">
                        <a href="{{route('home')}}"><img
                                style="background-position: center; margin-left: 80px; padding-top: 10px;"
                                src="{{asset('assets/frontend/upload/logo.png')}}"></a>
                    </div>
                    <div class="col-md-6 text-left" style="padding-top:25px ">
                        <div class="header-search">
                            <form method="get" action="{{route('search')}}">
                                <input class="input search-input" value="{{isset($search)?$search:''}}" type="text"
                                       name="search" placeholder="Enter your keyword">
                                <select class="input search-categories" name="category_id">
                                    <option value="">All Categories</option>
                                    @foreach($menudata as $menudatas)
                                        <option
                                            value="{{$menudatas->id}}" {{isset($category_id)&&$category_id==$menudatas->id?'selected':''}}>
                                            {{$menudatas->category}}
                                        </option>
                                    @endforeach
                                </select>
                                <button type="submit" class="search-btn"><i class="fa fa-search"></i></button>
                            </form>
                        </div>
                    </div><!-- end left -->
                    <div class="col-md-1 text-right" style="z-index: 99999">
                        <img
                            style="background-position: center; margin-right: 200px; margin-left: 50px;  padding-top: 20px;cursor: pointer" onclick="window.location.href='{{route('ShopCart')}}'"
                            src="{{asset('assets/frontend/upload/cart%20(7).ico')}}">
                        <div class="{{Cart::content()->count()==0?'hidden':''}} CartCount" style="
    background: red;
    color: white;
    padding: 5px;
    border-radius: 27%;
    margin-right: -39px;
    margin-top: -52px;
    width: 25px;
    float: right;
    z-index: 9990;
    text-align: center;
">{{Cart::content()->count()}}
                        </div>
                    </div><!-- end left -->
                    <div class="col-md-1 text-right">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="">
                                <img
                                    style="background-position: center; margin-left: 50px;  padding-top: 20px;"
                                    class="dropdown-toggle" data-toggle="dropdown" role="button"
                                    src="{{asset('assets/frontend/upload/login.png')}}">
                                @if(Auth::guard('user')->check())<p style="
                        font-weight: bold;
    white-space: nowrap;
    overflow: hidden;
    text-overflow: ellipsis;
    font-size: 17px;
    width: 130px;
">
                                    {{Auth::guard('user')->user()->name}}
                                </p>@endif
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div><!-- end row -->
    </div><!-- end container -->
</div>
<header class="header">
    <div class="container">
        <div class="row mb-5" style="padding-top: 12px; margin-left: 80px">
            <div class="col-md-12">
                <nav class="navbar yamm navbar-default">
                    <div class="container-full">
                        <div class="navbar-table">
                            <div class="navbar-cell">
                                <div class="navbar-header">

                                    <!--<div class="search">-->
                                    <!--<form>-->
                                    <!--<input   type="text" id="myInput">-->
                                    <!--<input type="submit" value="" >-->
                                    <!--</form>-->
                                    <!--</div>-->
                                    <div>
                                        <button type="button" class="navbar-toggle collapsed"
                                                data-toggle="collapse"
                                                data-target="#bs-example-navbar-collapse-2">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="fa fa-bars"></span>
                                        </button>
                                    </div>
                                </div><!-- end navbar-header -->
                            </div><!-- end navbar-cell -->
                            <div class="navbar-cell stretch pull-right">
                                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-2">
                                    <div class="navbar-cell">
                                        <ul class="nav navbar-nav navbar-left">
                                            <li class="dropdown has-submenu">
                                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                                   aria-expanded="false">
                                                    <i class="fa fa-list"></i> Shop by Category
                                                </a>
                                                @if($menudata->count() > 0)
                                                    <ul class="dropdown-menu start-left" role="menu"
                                                        style="background-color:  white; !important;box-shadow: 0 0 1px 0 black;">
                                                        @foreach($menudata as $menudatas)
                                                            @if($menudatas->category == 'Gifts')
                                                                <li>
                                                                    <a href="{{route('gift')}}" style="color:#666;">
                                                                        {{$menudatas->category}}
                                                                    </a>
                                                                </li>
                                                            @else
                                                                <li>
                                                                    <a href="#" class="dropdown-toggle"
                                                                       data-toggle="dropdown" style="color:#666;"
                                                                       role="button" aria-expanded="false">
                                                                        {{$menudatas->category}}
                                                                        @if($menudatas->subcategory->count() > 0)<span
                                                                            class="fa fa-angle-right"></span>@endif
                                                                    </a>
                                                                    @if($menudatas->subcategory->count() > 0)
                                                                        <ul class="dropdown-menu show-left" role="menu"
                                                                            style="width: 600px;background-color:  white; !important;box-shadow: 0 0 1px 0 black;">
                                                                            <li>
                                                                                <div style="padding: 10px">
                                                                                    @foreach($menudatas->subcategory as $subcategory)
                                                                                        <div class="col-sm-4">
                                                                                            <h3 style="color:#444;padding-bottom: 0;font-size: 16px;font-family: Roboto,serif">{{$subcategory->subcategory}}</h3>
                                                                                            @foreach($subcategory->brand as $brand)
                                                                                                <p class="linktext"
                                                                                                   onclick="window.location.href='{{route('shop','brand='.$brand->id)}}'">{{$brand->name}}</p>
                                                                                            @endforeach
                                                                                        </div>
                                                                                    @endforeach
                                                                                </div>

                                                                            </li>
                                                                        </ul>
                                                                    @endif
                                                                </li>
                                                            @endif
                                                        @endforeach
                                                    </ul>
                                                @endif
                                            </li>
                                        </ul>

                                        <ul class="nav navbar-nav navbar-left">
                                            <li>
                                                <a href="{{route('home')}}">Home</a>
                                            </li>
                                            <li>
                                                <a href="{{route('shop')}}">Shop</a>
                                            </li>
                                            <li>
                                                <a href="{{route('gift')}}">Gift</a>
                                            </li>
                                            <li>
                                                <a href="{{route('ShopCart')}}">Cart</a>
                                            </li>
                                            <li class="dropdown has-submenu">
                                                <a href="{{route('tracking')}}">Tracking</a>
                                            </li>
                                            <li>
                                                <a href="{{route('shop')}}">Contact</a>
                                            </li>
                                            @if(Auth::guard('user')->check())
                                                <li class="dropdown has-submenu">
                                                    <a href="{{route('OrderList')}}">Order List</a>
                                                </li>
                                                <li class="dropdown has-submenu">
                                                    <a href="/" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Logout</a>
                                                </li>
                                                <form id="logout-form" action="{{ route('logout') }}" method="POST"
                                                      style="display: none;">
                                                    @csrf
                                                    <input type="hidden" name="type" value="user">
                                                </form>
                                            @else
                                                <li class="dropdown has-submenu">
                                                    <a href="{{route('login.user')}}">Login</a>
                                                </li>
                                            @endif

                                        </ul>
                                    </div><!-- end navbar-cell -->
                                </div><!-- /.navbar-collapse -->
                            </div><!-- end navbar cell -->
                        </div><!-- end navbar-table -->
                    </div><!-- end container fluid -->
                </nav><!-- end navbar -->
            </div><!-- end col -->
        </div><!-- end row -->
    </div><!-- end container -->
</header>
