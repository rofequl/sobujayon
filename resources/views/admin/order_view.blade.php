@extends('admin.layout.app')
@section('title', 'Order Information')
@section('content')
    <div class="header-bg">

        @include('admin.inc.header')

        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <form class="float-right app-search">
                            <input type="text" placeholder="Search..." class="form-control">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                        <h4 class="page-title"><i class="dripicons-blog"></i>Order Details</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-20">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-12">
                                    @if($order->payment ==1)
                                        <button class="btn btn-success pull-right send-mail mx-3"
                                                onclick="window.location.href='{{route('AdminOrderPaid','unapproved='.base64_encode($order->id))}}'">
                                            <i class="mdi mdi-currency-usd"></i> Paid
                                        </button>
                                    @else
                                        <button class="btn btn-warning pull-right send-mail mx-3"
                                                onclick="window.location.href='{{route('AdminOrderPaid','approved='.base64_encode($order->id))}}'">
                                            <i class="mdi mdi-currency-usd"></i> Unpaid
                                        </button>
                                    @endif
                                    <select class="form-control pull-right" style="width: 200px"
                                            onchange="if (this.value) window.location.href='{{route('AdminOrderStatus')}}/?id={{$order->id}}&value='+this.value">
                                        <option value="" {{$order->status == 0?'selected':''}} disabled>Select Status
                                        </option>
                                        <option value="1" {{$order->status == 1?'selected':''}}>Order Confirmed</option>
                                        <option value="2" {{$order->status == 2?'selected':''}}>Picked and Packed
                                        </option>
                                        <option value="3" {{$order->status == 3?'selected':''}}>Shipped</option>
                                        <option value="4" {{$order->status == 4?'selected':''}}>Delivered</option>
                                        <option value="5" {{$order->status == 5?'selected':''}}>Rejected</option>
                                    </select>
                                </div>
                                <div class="col-12">
                                    <div class="logo">
                                        <!-- Image Logo -->
                                        <a href="{{route('admin')}}" class="logo">
                                            <!--<img src="assets/images/log.png" alt="" height="22" class="logo-small">-->
                                            <img src="{{asset('assets/admin/images/dabur.png')}}" alt="" height="60"
                                                 class="logo-large">
                                        </a>
                                        <!-- Text Logo -->
                                        <a href="{{route('admin')}}" style="color: #768a29  !important;font-size: 20px;font-weight: 700;
    letter-spacing: 3px;line-height: 60px;text-transform: uppercase;">
                                            সবুজায়ন
                                        </a>
                                    </div>
                                    {!! DNS1D::getBarcodeHTML($order->order_id, "EAN13") !!}
                                    <p class="text-dark" style="font-size: 15px">Order Code:
                                        {{$order->order_id}}</p>
                                </div>
                                <div class="col-12">
                                    <div class="row">
                                        <div class="col-md-9">
                                            <p class="h6"><i class="fa fa-user mr-2"></i> {{$order->user->name}}</p>
                                            <p class="h6"><i class="fa fa-phone mr-2"></i> {{$order->user->phone}}</p>
                                            <p class="h6"><i class="fa fa-globe mr-2"></i> {{$order->address}}</p>
                                        </div>
                                        <div class="col-md-3">
                                            <p class="h6"><span
                                                    class="mr-2">Date:</span> {{$order->created_at->format('d-M-Y')}}
                                            </p>
                                            <p class="h6"><span class="mr-2">Qty:</span> {{$order->total_product}}</p>
                                            <p class="h6"><span
                                                    class="mr-2">Total Price:</span></i> &#2547;{{$order->total_amount}}</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="table-responsive">
                                        <table style="text-align: center;" id="datatable-buttons"
                                               class="table table-striped table-bordered" cellspacing="0" width="100%">
                                            <thead>
                                            <tr>
                                                <th>Product Name</th>
                                                <th>Nursery</th>
                                                <th>Price</th>
                                                <th>Discount</th>
                                                <th>Qty</th>
                                                <th>Lift</th>
                                                <th>Delivery cost</th>
                                                <th>Lift cost</th>
                                                <th>Total cost</th>
                                                <th>Nursery Payment</th>
                                                <th>Total Income</th>
                                                <th>Photo</th>
                                                <th>Status</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($order->order_product as $product)
                                                <tr>
                                                    <td>{{$product->name}}</td>
                                                    <td>
                                                        Nursery Name: {{$product->product->nursery->nurserie_name}}<br>
                                                        owner Name: {{$product->product->nursery->name}}<br>
                                                        Phone: {{$product->product->nursery->phone}}<br>
                                                        Address: {{$product->product->nursery->nurserie_address}}<br>
                                                    </td>
                                                    <td>&#2547;{{$product->product->sales_price}}</td>
                                                    <td>&#2547;{{$product->product->discount}}</td>
                                                    <td>{{$product->qty}}</td>
                                                    <td>{{$product->lift}}</td>
                                                    <td>&#2547;{{$product->delivery_cost}}</td>
                                                    <td>&#2547;{{$product->lift_cost}}</td>
                                                    <td>&#2547;{{$product->total_price}}</td>
                                                    <td>&#2547;{{$product->nursery_payment}}</td>
                                                    <td>&#2547;{{$product->our_income}}</td>
                                                    <td><img class="rounded" alt="200x200"
                                                             src="{{asset('storage/tree/'.$product->product->image)}}"
                                                             width="80px" height="80px" data-holder-rendered="true">
                                                    </td>
                                                    <td>
                                                            @if($order->status == 1)
                                                                <span
                                                                    class="badge badge-secondary">Order Confirmed</span>
                                                            @elseif($order->status == 2)
                                                                <span
                                                                    class="badge badge-success">Picked and Packed</span>
                                                            @elseif($order->status == 3)
                                                                <span class="badge badge-primary">Shipped</span>
                                                            @elseif($order->status == 4)
                                                                <span class="badge badge-success">Delivered</span>
                                                            @elseif($order->status == 4)
                                                                <span class="badge badge-danger">Rejected</span>
                                                            @else
                                                                <span class="badge badge-dark p-1">Order Request</span>
                                                            @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        <nav class="pagi clearfix text-center">

                                        </nav>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->


@endsection

@push('style')
    <link href="{{asset('assets/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css"/>
@endpush

@push('script')
    <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>
    <script>
        @if(session()->has('message'))
        swal({
            title: 'Message',
            text: "{{ session()->get('message') }}",
            type: 'info',
            confirmButtonText: 'Ok'
        });
        @endif
    </script>


@endpush
