@extends('admin.layout.app')
@section('title','Add Nursery Admin')
@section('content')
    <div class="header-bg">

        @include('admin.inc.header')

        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <form class="float-right app-search">
                            <input type="text" placeholder="Search..." class="form-control">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                        <h4 class="page-title"><i class="dripicons-box"></i> NURSERY USER ADD</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <div class="container-fluid">

            <div class="row">
                <div class="col-12">
                    <div class="card m-b-20">
                        <div class="card-body">
                            @if(session()->has('message'))
                                <div class="alert alert-danger alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <form class="form-horizontal m-t-30" action="{{ route('register.nursery') }}"
                                  aria-label="{{ __('Register') }}" method="post">
                                @csrf
                                <div class="form-group">
                                    <label for="phone">Phone Number*</label>
                                    <input type="number" name="phone" value="{{ old('phone')}}"
                                           class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                           id="phone"
                                           placeholder="Enter phone number">
                                    @if ($errors->has('phone'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="username">Owner Name*</label>
                                    <input type="text" name="name" value="{{ old('name')}}"
                                           class="form-control {{ $errors->has('name') ? ' is-invalid' : '' }}"
                                           id="username"
                                           placeholder="Enter owner name">
                                    @if ($errors->has('name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="nurserie_name">Nursery Name</label>
                                    <input type="text" name="nurserie_name" value="{{ old('nurserie_name')}}"
                                           class="form-control {{ $errors->has('nurserie_name') ? ' is-invalid' : '' }}"
                                           id="nurserie_name"
                                           placeholder="Enter nursery name">
                                    @if ($errors->has('Nurserie_name'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nurserie_name') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="nurserie_address">Nursery Address</label>
                                    <textarea name="nurserie_address"
                                           class="form-control {{ $errors->has('nurserie_address') ? ' is-invalid' : '' }}"
                                           id="nurserie_address"
                                              placeholder="Enter nursery address">{{ old('nurserie_address')}}</textarea>
                                    @if ($errors->has('nurserie_address'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('nurserie_address') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group">
                                    <label for="userpassword">Password*</label>
                                    <input type="password" name="password"
                                           class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                           id="userpassword" placeholder="Enter password">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif
                                </div>
                                <div class="form-group">
                                    <label for="password_confirmation">Confirm Password*</label>
                                    <input type="password" name="password_confirmation"
                                           class="form-control {{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}"
                                           id="password_confirmation" placeholder="Confirm password">
                                    @if ($errors->has('password'))
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('password_confirmation') }}</strong>
                                    </span>
                                    @endif
                                </div>

                                <div class="form-group row m-t-20">
                                    <div class="col-12 text-right">
                                        <button class="btn btn-primary w-md waves-effect waves-light" type="submit">
                                            Register
                                        </button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div> <!-- end col -->
            </div> <!-- end row -->


        </div> <!-- end container -->
    </div>


@endsection

