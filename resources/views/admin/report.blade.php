@extends('admin.layout.app')
@section('title','Reporting')
@section('content')
    <div class="header-bg">

        @include('admin.inc.header')

        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <form class="float-right app-search">
                            <input type="text" placeholder="Search..." class="form-control">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                        <h4 class="page-title"><i class="dripicons-blog"></i>Reporting</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-20">
                        <div class="card-body">
                            <form method="get" action="{{route('AdminReport')}}">
                                <div class="form-group">
                                    <div>
                                        <div class="input-daterange input-group" id="date-range">
                                            <label class="col-sm-1 col-form-label">From</label>
                                            <input type="text" placeholder="mm/dd/yyyy"
                                                   value="{{isset($from1)?$from1:''}}" class="form-control"
                                                   name="start"/>
                                            <label class="col-sm-1 col-form-label">To</label>
                                            <input type="text" placeholder="mm/dd/yyyy" value="{{isset($to1)?$to1:''}}"
                                                   class="form-control"
                                                   name="end"/>
                                        </div>
                                    </div>
                                </div>
                                <div class="button-items">
                                    <button type="submit"
                                            class="btn btn-primary btn-lg btn-block waves-effect waves-light">Get Report
                                    </button>
                                </div>
                            </form>

                            @if(isset($order) && $order->count() > 0)
                                <div class="mt-5">
                                    <table id="datatable-buttons" class="table table-striped table-bordered mt-4"
                                           cellspacing="0"
                                           width="100%">
                                        <thead>
                                        <tr>
                                            <th>SL</th>
                                            <th>order ID</th>
                                            <th>Clint name</th>
                                            <th>Clint Phone</th>
                                            <th>Total Product</th>
                                            <th>Total Amount</th>
                                            <th>Order Date</th>
                                            <th>Status</th>
                                            <th> Action</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @php $no=1; $total = 0; @endphp
                                        @foreach($order as $orders)
                                            <tr>
                                                <td>{{$no}}</td>@php $no++ @endphp
                                                <td>{{$orders->order_id}}</td>
                                                <td>{{$orders->user->name}}</td>
                                                <td>{{$orders->user->phone}}</td>
                                                <td>{{$orders->total_product}}</td>@php $total += $orders->order_product->sum('our_income'); @endphp
                                                <td>{{$orders->order_product->sum('our_income')}}</td>
                                                <td>{{$orders->created_at->format('d-m-Y')}}</td>
                                                <td>
                                                    @if($orders->status == 1)
                                                        <span
                                                            class="badge badge-secondary">Order Confirmed</span>
                                                    @elseif($orders->status == 2)
                                                        <span
                                                            class="badge badge-success">Picked and Packed</span>
                                                    @elseif($orders->status == 3)
                                                        <span class="badge badge-primary">Shipped</span>
                                                    @elseif($orders->status == 4)
                                                        <span class="badge badge-success">Delivered</span>
                                                    @elseif($orders->status == 4)
                                                        <span class="badge badge-danger">Rejected</span>
                                                    @else
                                                        <span class="badge badge-dark p-1">Order Request</span>
                                                    @endif
                                                    @if($orders->payment == 1)
                                                        <span class="badge badge-success p-1">Paid</span>
                                                    @endif
                                                </td>
                                                <td>
                                                    <button class="btn btn-warning viewModal"
                                                            onclick="window.location.href='{{route('AdminOrderProduct',base64_encode($orders->id))}}'">
                                                        View
                                                    </button>
                                                </td>
                                            </tr>
                                        @endforeach
                                        <tr>
                                            <td colspan="5"><h4 class="text-right">Total:</h4></td>
                                            <td><h4>{{$total}}</h4></td>
                                            <td colspan="3"></td>
                                        </tr>
                                        </tbody>
                                    </table>
                                </div>
                            @endif

                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->


@endsection

@push('style')
    <!-- DataTables -->
    <link href="{{asset('assets/admin/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/admin/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <!-- Responsive datatable examples -->
    <link href="{{asset('assets/admin/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet"
    <link href="{{asset('assets/admin/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css')}}"
          rel="stylesheet"
    <link href="{{asset('assets/admin/plugins/select2/css/select2.min.css')}}" rel="stylesheet"
    <link href="{{asset('assets/admin/plugins/bootstrap-touchspin/css/jquery.bootstrap-touchspin.min.css')}}"
          rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css"
          rel="stylesheet" type="text/css"/>
@endpush

@push('script')
    <!-- Required datatable js -->
    <script src="{{asset('assets/admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{asset('assets/admin/plugins/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/jszip.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{asset('assets/admin/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{asset('assets/admin/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js')}}"></script>
    <script src="{{asset('assets/admin/pages/datatables.init.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/select2/js/select2.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/bootstrap-touchspin/js/jquery.bootstrap-touchspin.min.js')}}"></script>
    <script src="{{asset('assets/admin/pages/form-advanced.js')}}"></script>
    <script
        src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>

    <script>
        $('.input-daterange input').each(function () {
            $(this).datepicker();
        });
    </script>

@endpush
