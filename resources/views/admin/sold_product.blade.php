@extends('admin.layout.app')
@section('title',$title)
@section('content')
    <div class="header-bg">

        @include('admin.inc.header')

        <div class="container-fluid">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="page-title-box">
                        <form class="float-right app-search">
                            <input type="text" placeholder="Search..." class="form-control">
                            <button type="submit"><i class="fa fa-search"></i></button>
                        </form>
                        <h4 class="page-title"><i class="dripicons-blog"></i>{{$title}}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="wrapper">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                    <div class="card m-b-20">
                        <div class="card-body">
                            <table id="datatable-buttons" class="table table-striped table-bordered" cellspacing="0"
                                   width="100%">
                                <thead>
                                <tr>
                                    <th>SL</th>
                                    <th>order ID</th>
                                    <th>Clint name</th>
                                    <th>Clint Phone</th>
                                    <th>Total Product</th>
                                    <th>Total Amount</th>
                                    <th>Order Date</th>
                                    <th>Status</th>
                                    <th> Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @php $no=1 @endphp
                                @foreach($order as $orders)
                                    <tr>
                                        <td>{{$no}}</td>@php $no++ @endphp
                                        <td>{{$orders->order_id}}</td>
                                        <td>{{$orders->user->name}}</td>
                                        <td>{{$orders->user->phone}}</td>
                                        <td>{{$orders->total_product}}</td>
                                        <td>{{$orders->total_amount}}</td>
                                        <td>{{$orders->created_at->format('d-m-Y')}}</td>
                                        <td>
                                            @if($orders->status == 1)
                                                <span
                                                    class="badge badge-secondary">Order Confirmed</span>
                                            @elseif($orders->status == 2)
                                                <span
                                                    class="badge badge-success">Picked and Packed</span>
                                            @elseif($orders->status == 3)
                                                <span class="badge badge-primary">Shipped</span>
                                            @elseif($orders->status == 4)
                                                <span class="badge badge-success">Delivered</span>
                                            @elseif($orders->status == 4)
                                                <span class="badge badge-danger">Rejected</span>
                                            @else
                                                <span class="badge badge-dark p-1">Order Request</span>
                                            @endif
                                            @if($orders->payment == 1)
                                               <span class="badge badge-success p-1">Paid</span>
                                            @endif
                                        </td>
                                        <td>
                                            <button class="btn btn-warning viewModal"
                                                    onclick="window.location.href='{{route('AdminOrderProduct',base64_encode($orders->id))}}'">
                                                View
                                            </button>
                                        </td>
                                    </tr>
                                @endforeach


                                </tbody>
                            </table>


                        </div>
                    </div>
                </div> <!-- end col -->
            </div>
        </div> <!-- end col -->
    </div> <!-- end row -->


@endsection

@push('style')
    <!-- DataTables -->
    <link href="{{asset('assets/admin/plugins/datatables/dataTables.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/admin/plugins/datatables/buttons.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <!-- Responsive datatable examples -->
    <link href="{{asset('assets/admin/plugins/datatables/responsive.bootstrap4.min.css')}}" rel="stylesheet"
          type="text/css"/>
    <link href="{{asset('assets/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css"/>
@endpush

@push('script')
    <!-- Required datatable js -->
    <script src="{{asset('assets/admin/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/dataTables.bootstrap4.min.js')}}"></script>
    <!-- Buttons examples -->
    <script src="{{asset('assets/admin/plugins/datatables/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/jszip.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/pdfmake.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/vfs_fonts.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/buttons.html5.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/buttons.print.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/buttons.colVis.min.js')}}"></script>
    <!-- Responsive examples -->
    <script src="{{asset('assets/admin/plugins/datatables/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/datatables/responsive.bootstrap4.min.js')}}"></script>

    <!-- Datatable init js -->
    <script src="{{asset('assets/admin/pages/datatables.init.js')}}"></script>



@endpush
