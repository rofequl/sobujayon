@extends('mobile.layout.app')
@section('content')

    @if(Cart::content()->count() == 0)

        <!--================login Area =================-->
        <section class="emty_cart_area p_100">
            <div class="container">
                <div class="emty_cart_inner">
                    <i class="icon-handbag icons"></i>
                    <h3>Your Cart is Empty</h3>
                    <h4>back to <a href="{{route('shop')}}">shopping</a></h4>
                </div>
            </div>
        </section>
        <!--================End login Area =================-->

    @else
        <!--================Shopping Cart Area =================-->
        <section class="shopping_cart_area p_100">
            <div class="container">
                <div class="row">
                    <div class="col-12">
                        <div class="cart_product_list cart_items">
                            <h3 class="cart_single_title">Shopping Cart</h3>
                            <div class="table-responsive-md">
                                <table class="table">
                                    <thead>
                                    <tr>
                                        <th scope="col"></th>
                                        <th scope="col">Product</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Quantity</th>
                                        <th scope="col">Lift</th>
                                        <th scope="col">Delivery Cost</th>
                                        <th scope="col">Lift Cost</th>
                                        <th scope="col">Total</th>
                                    </tr>
                                    </thead>
                                    <tbody>

                                    @php $discount = 0; @endphp
                                    @foreach(Cart::content() as $row)
                                        @php $discount += (get_product_by_id($row->id)->discount * $row->qty); @endphp
                                        <tr>
                                            <th scope="row">
                                                <a href="{{route('ShopCartRemove','remove='.$row->rowId)}}">
                                                    <img src="{{asset('assets/mobile/img/icon/close-icon.png')}}"
                                                         alt="">
                                                </a>
                                            </th>
                                            <td style="padding: 10px">
                                                <div class="media">
                                                    <div class="d-flex">
                                                        <img
                                                            src="{{asset('storage/tree/'.get_product_by_id($row->id)->image)}}"
                                                            width="100" class="img-thumbnail img-fluid" style="max-width: none" alt="">
                                                    </div>
                                                    <div class="media-body">
                                                       <a href="{{route('SingleShop','product='.$row->id)}}"><h4>{{$row->name}}</h4></a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td style="padding: 10px"><p>
                                                    &#2547; @convert(get_product_by_id($row->id)->sales_price)</p>
                                            </td>
                                            <td style="padding: 10px">
                                                <form class="form-inline" name="CartUpdate{{$row->id}}"
                                                      action="{{route('CartUpdate')}}" method="post">
                                                    @csrf <input type="hidden" name="rowId" value="{{$row->rowId}}">
                                                    <div class="quantity">
                                                        <div class="custom">
                                                            <button
                                                                onclick="var result = document.getElementById('sst2{{$row->id}}'); var sst2 = result.value; if( !isNaN( sst2 ) &amp;&amp; sst2 > 1 ) result.value--;return false;"
                                                                class="reduced items-count" type="button"><i
                                                                    class="icon_minus-06"></i></button>
                                                            <input type="text" name="qty" id="sst2{{$row->id}}"
                                                                   maxlength="12"
                                                                   value="{{$row->qty}}"
                                                                   title="Quantity:" class="input-text qty">
                                                            <button
                                                                onclick="var result = document.getElementById('sst2{{$row->id}}'); var sst2 = result.value; if( !isNaN( sst2 )) result.value++;return false;"
                                                                class="increase items-count" type="button"><i
                                                                    class="icon_plus"></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                    <button type="submit"
                                                            class="input-group-addon py-3 btn"><i class="fa fa-repeat"
                                                                                                  aria-hidden="true"></i>
                                                    </button>
                                                </form>
                                            </td>
                                            <td style="padding: 10px"><p>{{$row->options->lift}}</p></td>
                                            <td style="padding: 10px"><p>&#2547;
                                                    @convert($row->options->delivery_cost)</p>
                                            </td>
                                            <td style="padding: 10px"><p>&#2547; @convert($row->options->lift_cost)</p>
                                            </td>
                                            <td style="padding: 10px"><p>
                                                    &#2547; @convert($row->price +
                                                    (get_product_by_id($row->id)->discount * $row->qty))</p>
                                            </td>
                                        </tr>
                                    @endforeach

                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 offset-md-8 mt-5">
                        <div class="total_amount_area">
                            <div class="cart_totals">
                                <div class="cart_total_inner">
                                    <ul>
                                        <li><a href="#"><span>Cart Subtotal</span>
                                                @php $total=0; @endphp
                                                @foreach(Cart::content() as $row) @php $total=$row->price+$total; @endphp @endforeach
                                                &#2547; @convert($total + $discount)
                                            </a></li>
                                        <li><a href="#"><span>Discount</span>&#2547; @convert($discount)</a></li>
                                        <li><a href="#"><span>Totals</span>
                                                @php $total=0; @endphp
                                                @foreach(Cart::content() as $row) @php $total=$row->price+$total; @endphp @endforeach
                                                &#2547; @convert($total)
                                            </a></li>
                                    </ul>
                                </div>
                                <button type="button"
                                        onclick="window.location.href='{{route('shop')}}'"
                                        class="btn btn-primary update_btn px-2">Continue Shop</button>
                                <button type="button"
                                        onclick="window.location.href='{{route('checkout')}}'"
                                        class="btn btn-primary checkout_btn">proceed to checkout</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Shopping Cart Area =================-->
    @endif

@endsection
