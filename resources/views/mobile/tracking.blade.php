@extends('mobile.layout.app')
@section('content')

    <!--================Track Area =================-->
    <section class="track_area p_100">
        <div class="container">
            <div class="track_inner">
                <div class="track_title">
                    <h2>Track Your Item</h2>
                    <p> Donec sed tortor eros. Mauris varius commodo nisi, quis vestibulum est pellen-tesque quis. Vivamus nulla arcu, </p>
                </div>
                <form class="track_form row" method="get" action="{{route('tracking')}}">
                    <div class="col-lg-12 form-group">
                        <label for="text">Order ID</label>
                        <input class="form-control" name="track" type="text" id="text">
                    </div>
                    <div class="col-lg-12 form-group">
                        <button type="submit" value="submit" class="btn subs_btn form-control">place order</button>
                    </div>
                </form>
            </div>
        </div>
    </section>
    <!--================End Track Area =================-->

    @if($order)
        <!--================Shopping Cart Area =================-->
        <section class="shopping_cart_area p_100 pt-0">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="cart_product_list">
                            <h3 class="cart_single_title">Client Order List</h3>
                            <div class="table-responsive-md">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th scope="col">SL</th>
                                        <th scope="col">Order Id</th>
                                        <th scope="col">Product Name</th>
                                        <th scope="col">Price</th>
                                        <th scope="col">Discount</th>
                                        <th scope="col">Qty</th>
                                        <th scope="col">Lift</th>
                                        <th scope="col">Delivery cost</th>
                                        <th scope="col">Lift cost</th>
                                        <th scope="col">Total cost</th>
                                        <th scope="col">Photo</th>
                                        <th scope="col">Status</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @php $sl = 1; @endphp
                                    @foreach($order as $orders)
                                        @foreach(get_order_product_by_order_id($orders->id) as $product)
                                            <tr>
                                                <th scope="row">
                                                    {{$sl}} @php $sl++; @endphp
                                                </th>
                                                <td><p>{{$orders->order_id}}</p></td>
                                                <td><p>{{$product->name}}</p></td>
                                                <td><p>&#2547; {{$product->product->sales_price}}</p></td>
                                                <td><p>&#2547; {{$product->product->discount}}</p></td>
                                                <td><p>{{$product->qty}}</p></td>
                                                <td><p>{{$product->lift}}</p></td>
                                                <td><p>&#2547; {{$product->delivery_cost}}</p></td>
                                                <td><p>&#2547; {{$product->lift_cost}}</p></td>
                                                <td><p>&#2547; {{$product->total_price}}</p></td>
                                                <td>
                                                    <img class="img-thumbnail" alt="200x200"
                                                         src="{{asset('storage/tree/'.get_product_by_id($product->product_id)->image)}}"
                                                         width="80px" height="80px" data-holder-rendered="true">
                                                </td>
                                                <td>
                                                    @if($orders->status == 1)
                                                        <span
                                                            class="label label-green">Order Confirmed</span>
                                                    @elseif($orders->status == 2)
                                                        <span
                                                            class="label label-mint">Picked and Packed</span>
                                                    @elseif($orders->status == 3)
                                                        <span class="label label-success">Shipped</span>
                                                    @elseif($orders->status == 4)
                                                        <span class="label label-primary">Delivered</span>
                                                    @elseif($orders->status == 5)
                                                        <span class="label label-danger">Rejected</span>
                                                    @else
                                                        <span class="label label-lime p-1">Order Request</span>
                                                    @endif
                                                    @if($orders->payment == 1)
                                                        <br><br><span class="label label-magneta p-1">Paid</span>
                                                    @endif
                                                </td>
                                            </tr>
                                        @endforeach
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--================End Shopping Cart Area =================-->
    @endif

@endsection
