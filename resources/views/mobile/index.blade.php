@extends('mobile.layout.app')
@section('content')


    <!--================Slider Area =================-->
    <section class="main_slider_area">
        <div class="container">
            <div id="main_slider" class="rev_slider" data-version="5.3.1.6">
                <ul>
                    <li data-index="rs-1587" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                        data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                        data-thumb="{{asset('assets/frontend/upload/22.jpg')}}" data-rotate="0"
                        data-saveperformance="off"
                        data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4=""
                        data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10=""
                        data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{asset('assets/frontend/upload/22.jpg')}}" alt="" data-bgposition="center center"
                             data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg"
                             data-no-retina>
                        <!-- LAYER NR. 1 -->
                        <div class="slider_text_box">
                            <div class="tp-caption tp-resizeme secand_text text-white"
                                 data-x="['right','right','right','center','center',]"
                                 data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['255','255','255','230','220']"
                                 data-fontsize="['48','48','48','48','36']"
                                 data-lineheight="['52','52','52','46']"
                                 data-width="['450','450','450','450','450']"
                                 data-height="none"
                                 data-whitespace="normal"
                                 data-type="text"
                                 data-responsive_offset="on"
                                 data-transform_idle="o:1;"
                                 data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                                 data-textAlign="['left','left','left','left','left','center']"
                            >Welcome To The <br/>Garden Shop
                            </div>
                        </div>
                    </li>
                    <li data-index="rs-1588" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                        data-hideslideonmobile="off" data-easein="default" data-easeout="default" data-masterspeed="300"
                        data-thumb="{{asset('assets/frontend/upload/22.jpg')}}" data-rotate="0"
                        data-saveperformance="off"
                        data-title="Creative" data-param1="01" data-param2="" data-param3="" data-param4=""
                        data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10=""
                        data-description="">
                        <!-- MAIN IMAGE -->
                        <img src="{{asset('assets/frontend/upload/22.jpg')}}" alt="" data-bgposition="center center"
                             data-bgfit="cover" data-bgrepeat="no-repeat" data-bgparallax="5" class="rev-slidebg"
                             data-no-retina>
                        <!-- LAYERS -->
                        <!-- LAYERS -->

                        <!-- LAYER NR. 1 -->
                        <div class="slider_text_box">
                            <div class="tp-caption tp-resizeme secand_text text-white"
                                 data-x="['right','right','right','center','center',]"
                                 data-hoffset="['0','0','0','0']"
                                 data-y="['top','top','top','top']" data-voffset="['255','255','255','230','220']"
                                 data-fontsize="['48','48','48','48','36']"
                                 data-lineheight="['52','52','52','46']"
                                 data-width="['450','450','450','450','450']"
                                 data-height="none"
                                 data-whitespace="normal"
                                 data-type="text"
                                 data-responsive_offset="on"
                                 data-transform_idle="o:1;"
                                 data-frames="[{&quot;delay&quot;:10,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;0&quot;,&quot;from&quot;:&quot;y:[100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;opacity:0;&quot;,&quot;mask&quot;:&quot;x:0px;y:[100%];s:inherit;e:inherit;&quot;,&quot;to&quot;:&quot;o:1;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;},{&quot;delay&quot;:&quot;wait&quot;,&quot;speed&quot;:1500,&quot;frame&quot;:&quot;999&quot;,&quot;to&quot;:&quot;y:[175%];&quot;,&quot;mask&quot;:&quot;x:inherit;y:inherit;s:inherit;e:inherit;&quot;,&quot;ease&quot;:&quot;Power2.easeInOut&quot;}]"
                                 data-textAlign="['left','left','left','left','left','center']"
                            >Designing Your <br/>Dream Garden
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </section>
    <!--================End Slider Area =================-->

    <!--================Our Latest Product Area =================-->
    <section class="our_latest_product">
        @foreach($brand as $brands)
            <div class="container">
                <div class="s_m_title">
                    <h2>{{$brands->name}}</h2>
                </div>
                <div class="l_product_slider owl-carousel">
                    @foreach(get_product_by_brand($brands->id) as $product)
                        <div class="item">
                            <div class="l_product_item">
                                <div class="l_p_img">
                                    <img src="{{asset('storage/tree/'.$product->image)}}" alt="">
                                </div>
                                <div class="l_p_text">
                                    <ul>
                                        <li class="p_icon"></li>
                                        <li><a class="add_cart_btn" href="{{route('SingleShop','product='.$product->id)}}">Add To Cart</a></li>
                                        <li class="p_icon"></li>
                                    </ul>
                                    <h4>{{$product->name->name}}</h4>
                                    <h5>
                                        <del>&#2547;{{$product->sales_price}}</del>
                                        &#2547;{{$product->sales_price - $product->discount}}
                                    </h5>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endforeach
    </section>
    <!--================End Our Latest Product Area =================-->

@endsection




