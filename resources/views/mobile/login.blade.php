@extends('mobile.layout.app')
@section('content')

    <!--================login Area =================-->
    <section class="login_area p_100">
        <div class="container">
            <div class="login_inner">
                <div id="message">
                    @if ($errors->any())
                        <ul class="alert alert-danger alert-dismissible">
                            @foreach ($errors->all() as $error)
                                <li>{{$error}}</li>
                            @endforeach
                        </ul>
                    @endif
                    @if(session()->has('message'))
                        <div class="alert alert-danger alert-dismissible">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                            {{ session()->get('message') }}
                        </div>
                    @endif
                </div>
                <div class="row justify-content-between">
                    <div class="col-lg-4">
                        <div class="login_title">
                            <h2>log in your account</h2>
                            <p>Let's contact now with our custom</p>
                        </div>
                        <form class="login_form row" method="POST" action='{{route('login.user')}}'>
                            @csrf
                            <div class="col-lg-12 form-group">
                                <input type="text" name="phone" id="phone"
                                       class="form-control {{ $errors->has('phone') ? ' is-invalid' : '' }}"
                                       placeholder="Phone">
                            </div>
                            <div class="col-lg-12 form-group">
                                <input type="password" name="password" id="password"
                                       class="form-control {{ $errors->has('password') ? ' is-invalid' : '' }}"
                                       placeholder="password">
                            </div>
                            <div class="col-lg-12 form-group">
                                <div class="creat_account">
                                    <input type="checkbox" id="f-option" name="remember">
                                    <label for="f-option">Keep me logged in</label>
                                    <div class="check"></div>
                                </div>
                            </div>
                            <div class="col-lg-12 form-group">
                                <button type="submit" value="submit" class="btn update_btn form-control">Login</button>
                            </div>
                        </form>
                    </div>
                    <div class="col-lg-7">
                        <div class="login_title">
                            <h2>create account</h2>
                            <p>Let's contact now with our custom</p>
                        </div>
                        <form class="login_form row" method="post" action='{{route('register.user')}}'>
                            @csrf
                            <div class="col-lg-6 form-group">
                                <input type="text" name="name" id="name" class="form-control"
                                       value="{{ old('name')}}" placeholder="Name">
                            </div>
                            <div class="col-lg-6 form-group">
                                <input type="tel" name="phone" id="phone" value="{{ old('phone')}}"
                                       class="form-control" placeholder="Phone">
                            </div>
                            <div class="col-lg-6 form-group">
                                <input type="password" name="password" id="password" class="form-control"
                                       placeholder="password">
                            </div>
                            <div class="col-lg-6 form-group">
                                <input type="password" name="password_confirmation" id="password"
                                       class="form-control" placeholder="confirm password">
                            </div>
                            <div class="col-lg-6 form-group">
                                <button type="submit" value="submit" class="btn subs_btn form-control">register now
                                </button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End login Area =================-->


@endsection
