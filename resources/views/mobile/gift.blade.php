@extends('mobile.layout.app')
@section('content')

    <!--================Categories Product Area =================-->
    <section class="no_sidebar_2column_area">
        <div class="container">
            <div class="showing_fillter">
                <div class="row m0">
                    <div class="first_fillter">
                        <h4>Showing {{($product->currentpage()-1)*$product->perpage()+1}}
                            to {{$product->currentpage()*$product->perpage()}} of {{$product->total()}} total</h4>
                    </div>
                    <div class="secand_fillter">
                        <h4>SORT BY :</h4>
                        <select class="selectpicker" id="selectTeam" onchange="refreshOrg()">
                            <option value="default">Default sorting</option>
                            <option value="new">Sort by newness</option>
                            <option value="low">Sort by price: low to high</option>
                            <option value="high">Sort by price: high to low</option>
                        </select>
                    </div>
                    <script>
                        function refreshOrg() {
                            window.location='{{route('gift')}}?type='+$('#selectTeam').val();
                        }
                    </script>
                </div>
            </div>
            <div class="two_column_product">
                <div class="row">
                    @foreach($product as $products)
                        <div class="col-lg-3 col-sm-6">
                            <div class="l_product_item">
                                <div class="l_p_img">
                                    <img class="img-fluid" src="{{asset('storage/tree/'.$products->image)}}" alt="">
                                </div>
                                <div class="l_p_text">
                                    <ul>
                                        <li class="p_icon"></li>
                                        <li><a class="add_cart_btn" href="{{route('SingleShop','product='.$products->id)}}">Add To Cart</a></li>
                                        <li class="p_icon"></li>
                                    </ul>
                                    <h4>{{$products->name->name}}</h4>
                                    <h5>
                                        <del>&#2547;{{$products->sales_price}}</del>
                                        &#2547;{{$products->sales_price - $products->discount}}
                                    </h5>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
                <nav aria-label="Page navigation example" class="pagination_area">
                    {{ $product->links() }}
                </nav>
            </div>
        </div>
    </section>
    <!--================End Categories Product Area =================-->


@endsection
