<style>
    .top_right li.cart a:before {
        content: '{{Cart::content()->count()}}';

    }
</style>


<!--================Top Header Area =================-->
<div class="header_top_area pb-3">
    <div class="container">
        <div class="row">
            <div class="col-lg-3">
                <div class="top_header_left">
                    <div class="selector">
                        <select class="language_drop" name="countries" onchange="refreshOrg2()" id="selectTeam2" style="width:300px;">
                            <option value='En' data-image="{{asset('assets/mobile/img/icon/flag-1.png')}}"
                                    data-imagecss="flag yt" data-title="English">English
                            </option>
                            <option value='Bn' data-image="{{asset('assets/mobile/img/icon/flag-2.png')}}"
                                    data-imagecss="flag yu" data-title="Bangladesh"
                                {{Session::has('locale') && Session('locale')=='Bn'?'selected':''}}>@lang('home.header1')
                            </option>
                        </select>
                    </div>
                    <script>
                        function refreshOrg2() {
                            window.location='{{url('language')}}/'+$('#selectTeam2').val();
                        }
                    </script>
                    <img src="{{asset('assets/frontend/upload/logo.png')}}" alt="Logo" class="mt-3 nayemEdit">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="top_header_middle text-center">
                    <a href="#"><i class="fa fa-phone"></i> @lang('home.header2'): <span>+84 987 654 321</span></a>
                    <a href="#"><i class="fa fa-envelope"></i> @lang('home.header3'): <span>support@yourdomain.com</span></a>
                    <form method="get" action="{{route('search')}}">
                        <div class="input-group mt-2">
                            <select class="form-control w-25" style="box-shadow: none" name="category_id">
                                <option value="">@lang('home.header4')</option>
                                @foreach($menudata as $menudatas)
                                    <option
                                        value="{{$menudatas->id}}" {{isset($category_id)&&$category_id==$menudatas->id?'selected':''}}>
                                        {{$menudatas->category}}
                                    </option>
                                @endforeach
                            </select>
                            <input type="text" class="form-control shadow-none w-75" style="box-shadow: none"
                                   name="search"
                                   placeholder="@lang('home.header15')" aria-label="Search"
                                   value="{{isset($search)?$search:''}}">
                            <span class="input-group-btn">
                                <button class="btn btn-secondary shadow-none" style="box-shadow: none" type="submit"><i
                                        class="icon-magnifier"></i></button>
                                </span>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="top_right_header">
                    <ul class="header_social">
                        <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                        <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                        <li><a href="#"><i class="fa fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                    </ul>
                    @php $total=0; @endphp
                    @foreach(Cart::content() as $row) @php
                        $total=$row->price+$total;
                    @endphp @endforeach
                    <ul class="top_right mt-0">
                        <li class="cart"><a href="{{route('ShopCart')}}"><i class="icon-handbag icons"></i></a></li>
                        <li class="h_price">
                            <a href="{{route('ShopCart')}}">&#2547; @convert($total)</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!--================End Top Header Area =================-->

<!--================Menu Area =================-->
<header class="shop_header_area">
    <div class="container">
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
            <a class="navbar-brand" href="{{route('home')}}"><img src="{{asset('assets/frontend/upload/logo.png')}}"
                                                                  alt=""></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent"
                    aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav categories">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle mx-5" href="#" role="button" data-toggle="dropdown"
                           aria-haspopup="true" aria-expanded="false">
                            @lang('home.header5') <i class="fa fa-angle-down" aria-hidden="true"></i>
                        </a>
                        @if($menudata->count() > 0)
                            <ul class="dropdown-menu text-left ml-5 nayemDropdown" style="border: 1px solid gray;" aria-labelledby="navbarDropdownMenuLink" id="navbarDropdownMenuLink">
                                @foreach($menudata as $menudatas)
                                    @if($menudatas->category == 'Gifts')
                                        <li class="mr-0">
                                            <a class="dropdown-item text-left px-2" href="{{route('gift')}}" style="line-height: 50px">
                                                {{$menudatas->category}}
                                            </a>
                                        </li>
                                    @else
                                        <li class="mr-0 dropdown-submenu"><a class="dropdown-item text-left px-2" style="line-height: 40px"
                                                            href="#">{{$menudatas->category}}
                                                 @if($menudatas->subcategory->count() > 0)<span
                                                    class="text-right fa fa-angle-right"></span>@endif</a>
                                            @if($menudatas->subcategory->count() > 0)
                                                <ul class="dropdown-menu ml-1" role="menu" style="width: 600px">
                                                    <li>
                                                        <div style="padding: 10px" class="text-left row">
                                                            @foreach($menudatas->subcategory as $subcategory)
                                                                <div class="col-sm-4">
                                                                    <h3 class="py-1" style="color:#444;font-size: 16px;font-family: Roboto,serif">{{$subcategory->subcategory}}</h3>
                                                                    @foreach($subcategory->brand as $brand)
                                                                        <p class="linktext py-1"
                                                                           onclick="window.location.href='{{route('shop','brand='.$brand->id)}}'">{{$brand->name}}</p>
                                                                    @endforeach
                                                                </div>
                                                            @endforeach
                                                        </div>

                                                    </li>
                                                </ul>
                                            @endif
                                        </li>
                                    @endif
                                @endforeach
                            </ul>
                        @endif
                    </li>
                </ul>
                <ul class="navbar-nav">
                    <li class="nav-item"><a class="nav-link" href="{{route('home')}}">@lang('home.header6')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('shop')}}">@lang('home.header7')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('gift')}}">@lang('home.header8')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('ShopCart')}}">@lang('home.header9')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('tracking')}}">@lang('home.header10')</a></li>
                    <li class="nav-item"><a class="nav-link" href="{{route('shop')}}">@lang('home.header11')</a></li>
                    @if(Auth::guard('user')->check())
                        <li class="nav-item"><a class="nav-link" href="{{route('OrderList')}}">@lang('home.header12')</a></li>
                        <li class="nav-item"><a class="nav-link" href="/"
                                                onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">@lang('home.header13')</a></li>
                    @else
                        <li class="nav-item"><a class="nav-link" href="{{route('OrderList')}}">@lang('home.header14')</a></li>
                    @endif
                    <form id="logout-form" action="{{ route('logout') }}" method="POST"
                          style="display: none;">
                        @csrf
                        <input type="hidden" name="type" value="user">
                    </form>
                </ul>
            </div>
        </nav>
    </div>
</header>
<!--================End Menu Area =================-->
