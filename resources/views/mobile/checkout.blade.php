@extends('mobile.layout.app')
@section('content')

    <!--================Register Area =================-->
    <section class="register_area p_100">
        <div class="container">
            <div class="register_inner">

                <div class="row">
                    <div class="col-lg-7">
                        <div class="billing_details">
                            <h2 class="reg_title">Shipping Address</h2>
                            @if ($errors->any())
                                @foreach ($errors->all() as $error)
                                    <div class="alert alert-warning alert-dismissible">
                                        <a href="#" class="close" data-dismiss="alert"
                                           aria-label="close">&times;</a>
                                        {{$error}}
                                    </div>
                                @endforeach
                            @endif
                            @if(session()->has('message'))
                                <div class="alert alert-success alert-dismissible">
                                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                            <form class="billing_inner row" method="post" action="{{route('CheckoutStore')}}"
                                  id="checkout-form">
                                @csrf
                                <div class="col-lg-12">
                                    <div class="form-group">
                                        <textarea id="textarea" name="address" class="form-control" maxlength="1000"
                                                  rows="3"
                                                  placeholder="Write Your Details Address"></textarea>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>

                    @php $total=0; $discount = 0; @endphp
                    @foreach(Cart::content() as $row) @php
                        $total=$row->price+$total;
                        $discount += (get_product_by_id($row->id)->discount * $row->qty);
                    @endphp @endforeach

                    <div class="col-lg-5">
                        <div class="order_box_price">
                            <h2 class="reg_title">Your Order</h2>
                            <div class="payment_list">
                                <div class="price_single_cost">
                                    <h5>Mens Casual Shirt <span>&#2547; @convert($total + $discount)</span></h5>
                                    <h5>Mens Casual Shirt <span>&#2547; @convert($discount)</span></h5>
                                    <h3><span class="normal_text">Order Totals</span>
                                        <span>&#2547; @convert($total)</span></h3>
                                </div>
                            </div>
                            <button type="submit" value="submit"
                                    onclick="document.getElementById('checkout-form').submit();"
                                    class="btn subs_btn form-control">place In order
                            </button>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>
    <!--================End Register Area =================-->

@endsection
