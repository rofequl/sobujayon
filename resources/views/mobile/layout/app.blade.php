<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}"/>

    <link rel="icon" href="{{asset('assets/frontend/images/icon.png')}}" type="image/x-icon"/>
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->

    <title>সবুজায়ন</title>
    <link rel="shortcut icon" href="{{asset('assets/frontend/images/icon.png')}}" type="image/x-icon">
    <link rel="apple-touch-icon" href="{{asset('assets/frontend/images/apple-touch-icon.png')}}"/>
    <link rel="apple-touch-icon" sizes="57x57" href="{{asset('assets/frontend/images/apple-touch-icon-57x57.png')}}">
    <link rel="apple-touch-icon" sizes="72x72" href="{{asset('assets/frontend/images/apple-touch-icon-72x72.png')}}">
    <link rel="apple-touch-icon" sizes="76x76" href="{{asset('assets/frontend/images/apple-touch-icon-76x76.png')}}">
    <link rel="apple-touch-icon" sizes="114x114"
          href="{{asset('assets/frontend/images/apple-touch-icon-114x114.png')}}">
    <link rel="apple-touch-icon" sizes="120x120"
          href="{{asset('assets/frontend/images/apple-touch-icon-120x120.png')}}">
    <link rel="apple-touch-icon" sizes="144x144"
          href="{{asset('assets/frontend/images/apple-touch-icon-144x144.png')}}">
    <link rel="apple-touch-icon" sizes="152x152"
          href="{{asset('assets/frontend/images/apple-touch-icon-152x152.png')}}">


    <!-- Icon css link -->
    <link href="{{asset('assets/mobile/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/mobile/vendors/line-icon/css/simple-line-icons.css')}}" rel="stylesheet">
    <link href="{{asset('assets/mobile/vendors/elegant-icon/style.css')}}" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="{{asset('assets/mobile/css/bootstrap.min.css')}}" rel="stylesheet">

    <!-- Rev slider css -->
    <link href="{{asset('assets/mobile/vendors/revolution/css/settings.css')}}" rel="stylesheet">
    <link href="{{asset('assets/mobile/vendors/revolution/css/layers.css')}}" rel="stylesheet">
    <link href="{{asset('assets/mobile/vendors/revolution/css/navigation.css')}}" rel="stylesheet">

    <!-- Extra plugin css -->
    <link href="{{asset('assets/mobile/vendors/owl-carousel/owl.carousel.min.css')}}" rel="stylesheet">
    <link href="{{asset('assets/mobile/vendors/bootstrap-selector/css/bootstrap-select.min.css')}}" rel="stylesheet">

    <link href="{{asset('assets/mobile/css/style.css')}}" rel="stylesheet">
    <link href="{{asset('assets/mobile/css/responsive.css')}}" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    @stack('style')
</head>
<body>


@include('mobile.inc.header')

@yield('content')

@include('mobile.inc.footer')

<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="{{asset('assets/mobile/js/jquery-3.2.1.min.js')}}"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="{{asset('assets/mobile/js/popper.min.js')}}"></script>
<script src="{{asset('assets/mobile/js/bootstrap.min.js')}}"></script>
<!-- Rev slider js -->
<script src="{{asset('assets/mobile/vendors/revolution/js/jquery.themepunch.tools.min.js')}}"></script>
<script src="{{asset('assets/mobile/vendors/revolution/js/jquery.themepunch.revolution.min.js')}}"></script>
<script src="{{asset('assets/mobile/vendors/revolution/js/extensions/revolution.extension.actions.min.js')}}"></script>
<script src="{{asset('assets/mobile/vendors/revolution/js/extensions/revolution.extension.video.min.js')}}"></script>
<script
    src="{{asset('assets/mobile/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<script
    src="{{asset('assets/mobile/vendors/revolution/js/extensions/revolution.extension.layeranimation.min.js')}}"></script>
<script
    src="{{asset('assets/mobile/vendors/revolution/js/extensions/revolution.extension.navigation.min.js')}}"></script>
<script
    src="{{asset('assets/mobile/vendors/revolution/js/extensions/revolution.extension.slideanims.min.js')}}"></script>
<!-- Extra plugin css -->
<script src="{{asset('assets/mobile/vendors/counterup/jquery.waypoints.min.js')}}"></script>
<script src="{{asset('assets/mobile/vendors/counterup/jquery.counterup.min.js')}}"></script>
<script src="{{asset('assets/mobile/vendors/owl-carousel/owl.carousel.min.js')}}"></script>
<script src="{{asset('assets/mobile/vendors/bootstrap-selector/js/bootstrap-select.min.js')}}"></script>
<script src="{{asset('assets/mobile/vendors/image-dropdown/jquery.dd.min.js')}}"></script>
<script src="{{asset('assets/mobile/js/smoothscroll.js')}}"></script>
<script src="{{asset('assets/mobile/vendors/isotope/imagesloaded.pkgd.min.js')}}"></script>
<script src="{{asset('assets/mobile/vendors/isotope/isotope.pkgd.min.js')}}"></script>
<script src="{{asset('assets/mobile/vendors/magnify-popup/jquery.magnific-popup.min.js')}}"></script>
<script src="{{asset('assets/mobile/vendors/vertical-slider/js/jQuery.verticalCarousel.js')}}"></script>
<script src="{{asset('assets/mobile/vendors/jquery-ui/jquery-ui.js')}}"></script>
<script src="{{asset('assets/mobile/js/theme.js')}}"></script>
@stack('script')
</body>
</html>
