@extends('mobile.layout.app')
@section('content')

    <!--================Product Details Area =================-->
    <section class="product_details_area">
        <div class="container">
            <div class="row">
                <div class="col-lg-4" id="ImageZoom">
                    <div class="exzoom hidden" id="exzoom">
                        <div class="exzoom_img_box">
                            <ul class='exzoom_img_ul'>
                                <li><img src="{{asset('storage/tree/'.$product->image)}}"/></li>
                                <li><img src="{{asset('storage/tree/'.$product->image2)}}"/></li>
                                <li><img src="{{asset('storage/tree/'.$product->image3)}}"/></li>
                                <li><img src="{{asset('storage/tree/'.$product->image4)}}"/></li>
                            </ul>
                        </div>
                        <div class="exzoom_nav"></div>
                        <p class="exzoom_btn">
                            <a href="javascript:void(0);" class="exzoom_prev_btn"> < </a>
                            <a href="javascript:void(0);" class="exzoom_next_btn"> > </a>
                        </p>
                    </div>

                </div>
                <div class="col-lg-8">
                    <div class="product_details_text">
                        <h3>{{$product->name->name}}</h3>
                        <div class="add_review pl-0">
                            <a href="#">Color: {{$product->color}}</a>
                            <a href="#">{{$product->qty}} product has</a>
                        </div>
                        <h4>&#2547;{{$product->sales_price - $product->discount}}</h4>
                        <p>
                            Item weight: {{$product->item_weight->weight}}<br>
                            Price: {{$product->sales_price}}<br>
                            Discount: {{$product->discount}}<br>
                            Height & Width: {{$product->height->height}}
                            & {{$product->width->width}}<br>
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="p_color">
                                    <div class="row">
                                        <div class="col-3">
                                            <h4 class="p_d_title" style="line-height: 45px">Lift: </h4>
                                        </div>
                                        <div class="col-9 quantity mt-0">
                                            <div class="custom">
                                                <button
                                                    onclick="var result = document.getElementById('lift'); var lift = result.value; if( !isNaN( lift ) &amp;&amp; lift > 1 ) result.value--;return false;"
                                                    class="reduced items-count AllEdit" type="button"><i
                                                        class="icon_minus-06 "></i></button>
                                                <input type="text" name="qty" id="lift" maxlength="12" value="1"
                                                       title="Quantity:"
                                                       class="input-text qty" readonly>
                                                <button
                                                    onclick="var result = document.getElementById('lift'); var lift = result.value; if( !isNaN( lift )) result.value++;return false;"
                                                    class="increase items-count AllEdit" type="button"><i
                                                        class="icon_plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="p_color">
                                    <div class="row">
                                        <div class="col-3">
                                            <h4 class="p_d_title" style="line-height: 45px">Qty: </h4>
                                        </div>
                                        <div class="col-9 quantity mt-0">
                                            <div class="custom">
                                                <button
                                                    onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty ) &amp;&amp; qty > 1 ) result.value--;return false;"
                                                    class="reduced items-count AllEdit" type="button"><i
                                                        class="icon_minus-06"></i></button>
                                                <input type="text" name="qty" id="qty" maxlength="12" value="1"
                                                       title="Quantity:"
                                                       class="input-text qty" readonly>
                                                <button
                                                    onclick="var result = document.getElementById('qty'); var qty = result.value; if( !isNaN( qty )) result.value++;return false;"
                                                    class="increase items-count AllEdit" type="button"><i
                                                        class="icon_plus"></i>
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-5">
                                <div class="quantity">
                                    <button class="add_cart_btn add-cart" id="{{$product->id}}">add to cart</button>
                                </div>
                                <div class="shareing_icon row">
                                    <div class="col-4"><h5>share :</h5></div>
                                    <div class="col-8">
                                        {!! Share::currentPage()->facebook()->reddit()->twitter()->linkedin()->whatsapp() !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-7 pr-5 login_form">
                                <div class="row" style="margin-top: 30px">
                                    <div class="col-md-6 text-right" style="line-height: 43px;">
                                        Delivery Cost:
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="text" id="delivery_cost"
                                               class="form-control form-control-sm" value=""
                                               readonly="">
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-6 text-right" style="line-height: 43px;">
                                        Lift Cost:
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="text" id="lift_cost" class="form-control form-control-sm"
                                               value=""
                                               readonly="">
                                    </div>
                                </div>
                                <div class="row" style="margin-top: 10px">
                                    <div class="col-md-6 text-right" style="line-height: 43px;">
                                        Total Cost:
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <input type="text" id="total_cost" class="form-control" value=""
                                               readonly="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Product Details Area =================-->

    <!--================Product Description Area =================-->
    <section class="product_description_area">
        <div class="container">
            <nav class="tab_menu">
                <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab"
                       aria-controls="nav-home" aria-selected="true">Product Description</a>
                    <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab"
                       aria-controls="nav-profile" aria-selected="false">Product Details</a>
                    <a class="nav-item nav-link" id="nav-contact-tab" data-toggle="tab" href="#nav-contact" role="tab"
                       aria-controls="nav-contact" aria-selected="false">What you will get?</a>
                    <a class="nav-item nav-link" id="nav-info-tab" data-toggle="tab" href="#nav-info" role="tab"
                       aria-controls="nav-info" aria-selected="false">Product Video</a>
                    <a class="nav-item nav-link" id="nav-gur-tab" data-toggle="tab" href="#nav-gur" role="tab"
                       aria-controls="nav-gur" aria-selected="false">Shipping</a>
                </div>
            </nav>
            <div class="tab-content" id="nav-tabContent">
                <div class="tab-pane fade show active" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                    <p><img src="{{asset('storage/tree/convert/'.$product->image)}}"
                            class="alignleft" alt="" width="100px">
                        {{$product->short_description}} </p>
                    <br>
                    <p>{{$product->long_description}}</p>
                </div>
                <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                    <div class="row">
                        <div class="col-md-6">
                            <p style="padding-bottom: 0">
                                Category: {{$product->category->category}}</p>
                            <p style="padding-bottom: 0">
                                Subcategory: {{$product->subcategory->subcategory}}</p>
                            <p style="padding-bottom: 0">
                                Color: {{$product->color}}</p>
                            <p style="padding-bottom: 0">
                                Age: {{$product->age->age}}</p>
                            <p style="padding-bottom: 0">
                                With fruit: {{$product->with_fruit}}</p>
                            <p style="padding-bottom: 0">
                                With flower: {{$product->with_flower}}</p>
                        </div>
                        <div class="col-md-6">
                            <p style="padding-bottom: 0">Origin
                                Country: {{$product->origin_country->country}}</p>
                            <p style="padding-bottom: 0">
                                Temperature: {{$product->temperature->temperature}}</p>
                            <p style="padding-bottom: 0">
                                Height: {{$product->height->height}}</p>
                            <p style="padding-bottom: 0">
                                Width: {{$product->width->width}}</p>
                            <p style="padding-bottom: 0">
                                Gift: {{$product->gift}}</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-contact" role="tabpanel" aria-labelledby="nav-contact-tab">
                    <div class="row">
                        <div class="col-md-12">
                            <p style="padding-bottom: 0">
                                {{$product->what_you_will_get}}</p>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-info" role="tabpanel" aria-labelledby="nav-info-tab">
                    <div class="row">
                        <div class="col-md-12">
                            @php
                                $url=$product->video;

                                parse_str( parse_url( $url, PHP_URL_QUERY ), $my_array_of_vars );
                            @endphp
                            <iframe width="560" height="315"
                                    src="https://www.youtube.com/embed/{{isset($my_array_of_vars['v'])?$my_array_of_vars['v']:''}}"
                                    frameborder="0" gesture="media"
                                    allow="autoplay; encrypted-media" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="tab-pane fade" id="nav-gur" role="tabpanel" aria-labelledby="nav-gur-tab">
                    <div class="row">
                        <div class="col-md-12">
                            <p style="padding-bottom: 0">
                                Modify live shipping rates: surcharge, deduct or override rates,
                                provided by carriers. E.g. surcharge rates for delivery to Hawaii
                                and Alaska, correct rates for LTL freights.</p>

                            <ul style="list-style: circle;padding: 0 15px" class="list-group">
                                <li class="list-group-item">Shipping rules based on order, product and customer
                                    attributes
                                </li>
                                <li class="list-group-item">Modify rates both for the whole cart and a single product
                                </li>
                                <li class="list-group-item">Set up shipping based on customer groups and store views
                                </li>
                                <li class="list-group-item">Change delivery prices both by flat amount and by percent
                                </li>
                                <li class="list-group-item">Provide correct rates for LTL freights</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--================End Product Details Area =================-->

    <!--================End Related Product Area =================-->
    @if($recent_product->count()>2)
        <section class="related_product_area">
            <div class="container">
                <div class="related_product_inner">
                    <h2 class="single_c_title">Recently Viewed Product</h2>
                    <div class="row">
                        @foreach($recent_product as $recent_products)
                            <div class="col-lg-3 col-sm-6">
                                <div class="l_product_item">
                                    <div class="l_p_img">
                                        <img class="img-fluid" src="{{asset('storage/tree/'.$recent_products->image)}}"
                                             alt="">
                                    </div>
                                    <div class="l_p_text">
                                        <ul>
                                            <li class="p_icon"></li>
                                            <li><a class="add_cart_btn"
                                                   href="{{route('SingleShop','product='.$recent_products->id)}}">Add To
                                                    Cart</a></li>
                                            <li class="p_icon"></li>
                                        </ul>
                                        <h4>{{$recent_products->name->name}}</h4>
                                        <h5>
                                            <del>&#2547;{{$recent_products->sales_price}}</del>
                                            &#2547;{{$recent_products->sales_price - $recent_products->discount}}</h5>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </section>
    @endif
    <!--================End Related Product Area =================-->

@endsection

@push('style')
    <link href="{{asset('assets/sweetalert/sweetalert.css')}}" rel="stylesheet" type="text/css"/>
    <link href="{{asset('assets/exzoom/jquery.exzoom.css')}}" rel="stylesheet" type="text/css"/>
    <style>
        #exzoom {
            width: 360px;
            /*height: 400px;*/
        }
        .exzoom .exzoom_btn a{
            height: 73px;
            line-height: 74px;
            top: -75px;

        }

        #ImageZoom {
            max-width: 960px;
        }

        .hidden {
            display: none;
        }
    </style>
@endpush
@push('script')
    <script src="https://unpkg.com/imagesloaded@4/imagesloaded.pkgd.min.js"></script>
    <script src="{{asset('assets/sweetalert/sweetalert.js')}}"></script>
    <script src="{{asset('assets/exzoom/jquery.exzoom.js')}}"></script>
    <script src="{{ asset('js/share.js') }}"></script>
    <script>
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');


        $('#ImageZoom').imagesLoaded(function () {
            $("#exzoom").exzoom({
                "autoPlay": true,
                "navWidth": 74,
                "navHeight": 73,
                "navItemNum": 4,
                "navItemMargin": 7,
                "navBorder": 1,

            });
            $("#exzoom").removeClass('hidden');
        });

        $(document).ready(function () {

            $(document).on('click', '.add-cart', function () {
                event.preventDefault();
                let id = $(this).attr('id');
                let lift = parseInt($("#lift").val());
                var $input = $("#qty");
                var value = parseInt($input.val());
                if (isNaN(lift) || isNaN(value) || lift <= 0 || value <= 0) {
                    swal({
                        title: "",
                        text: "Please enter product quantity and building floor.",
                        type: "warning",
                    });
                    return;
                }
                $.ajax({
                    url: '{{route('AddCart')}}',
                    type: 'post',
                    data: {_token: CSRF_TOKEN, id: id, qty: value, lift: lift},
                    success: function (response) {
                        if (response == 1) {
                            swal({
                                title: "",
                                text: "Item is already in your cart",
                                type: "warning",
                            });
                        } else if (response.action == 3) {
                            swal({
                                title: "",
                                text: "Item has been moved to cart",
                                type: "success",
                            });
                            let c = response.qty;
                            var add = '<style>.top_right li.cart a:before{content:"' + c + '"!important;}</style>';
                            $('body').append(add);
                        } else {
                            swal({
                                title: "Error",
                                text: "Something wrong, please try again later",
                                type: "warning",
                            });
                        }
                    }
                });

            });
            total_cost();

            function total_cost() {
                let quantity = parseInt($("#qty").val());
                let lift = parseInt($("#lift").val());
                let id = parseInt('{{$product->id}}');
                $.ajax({
                    url: '{{route('TotalCost')}}',
                    type: 'post',
                    dataType: 'json',
                    data: {_token: CSRF_TOKEN, id: id, lift: lift, qty: quantity},
                    success: function (response) {
                        $('#delivery_cost').val(response.delivery_cost);
                        $('#lift_cost').val(response.lift_cost);
                        $('#total_cost').val(response.total_cost);
                    }
                });
            }

            $('.qty').on('change paste keyup', function () {
                total_cost();
            });
            $('.AllEdit').on('click', function () {
                total_cost();
            });
        });


    </script>
@endpush
