-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 05, 2020 at 07:37 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sobujayon`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_super` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`id`, `name`, `email`, `password`, `is_super`, `remember_token`, `created_at`, `updated_at`) VALUES
(3, 'admin', 'admin@me.com', '$2y$10$c2HfkNhWIQqyte6BNbDTnuNd4meAGIhZ3NAgtCjyuIt9zZZJHTyai', 0, 'T3ub2jukb89UvIVQHYgBQDAWLZ9oDDHiugoqe0DpMZU2rzbWLbOBAenoVw7h', '2019-07-27 07:02:13', '2019-07-27 07:02:13');

-- --------------------------------------------------------

--
-- Table structure for table `ages`
--

CREATE TABLE `ages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `age` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `ages`
--

INSERT INTO `ages` (`id`, `age`, `created_at`, `updated_at`) VALUES
(1, '5', NULL, NULL),
(2, '6', NULL, NULL),
(3, '7', NULL, NULL),
(4, '3', NULL, NULL),
(5, '1', NULL, NULL),
(6, '2', NULL, NULL),
(7, '4', NULL, NULL),
(8, '8', NULL, NULL),
(9, '9', NULL, NULL),
(10, '10', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `brands`
--

CREATE TABLE `brands` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subcategory_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `brands`
--

INSERT INTO `brands` (`id`, `subcategory_id`, `name`, `status`, `created_at`, `updated_at`) VALUES
(1, '9', 'Air Plants', '1', '2019-08-17 11:01:40', '2019-09-03 17:39:41'),
(2, '9', 'Avenue Trees', '0', '2019-08-17 11:02:09', '2019-08-17 11:02:09'),
(3, '9', 'Bamboos Plants', '1', '2019-08-17 11:02:24', '2019-08-20 14:01:18'),
(4, '9', 'Cactus n Succulents', '0', '2019-08-17 11:02:33', '2019-08-19 16:10:55'),
(5, '15', 'Belal', '0', '2019-08-20 13:54:07', '2019-08-20 13:54:07');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `category`, `created_at`, `updated_at`) VALUES
(5, 'Plants', '2019-08-17 05:19:44', '2019-08-17 05:19:44'),
(6, 'Plantscaping', '2019-08-17 05:19:59', '2019-08-17 05:19:59'),
(7, 'Seeds', '2019-08-17 05:20:09', '2019-08-17 05:20:09'),
(8, 'Bulbs', '2019-08-17 05:20:19', '2019-08-17 05:20:19'),
(9, 'Pots & Planters', '2019-08-17 05:20:36', '2019-08-17 05:20:36'),
(10, 'Pebbles', '2019-08-17 05:20:46', '2019-08-17 05:20:46'),
(11, 'Soil & Fertilizers', '2019-08-17 05:20:58', '2019-08-17 05:20:58'),
(12, 'Gifts', '2019-08-17 05:21:15', '2019-08-17 05:21:15'),
(13, 'Top 10 plants', '2019-08-17 05:21:30', '2019-08-17 05:21:30'),
(14, 'Garden Accessories', '2019-08-17 05:21:41', '2019-08-17 05:21:41'),
(15, 'Apteaui a rskna', '2019-08-20 13:53:17', '2019-08-20 13:53:17');

-- --------------------------------------------------------

--
-- Table structure for table `delivery_costs`
--

CREATE TABLE `delivery_costs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `height` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `below_5` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `below_10` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `below_20` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `up_20` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `delivery_costs`
--

INSERT INTO `delivery_costs` (`id`, `height`, `weight`, `below_5`, `below_10`, `below_20`, `up_20`, `created_at`, `updated_at`) VALUES
(1, '10', '10', '20', '18', '15', '12', NULL, NULL),
(2, '20', '20', '25', '23', '20', '18', NULL, NULL),
(3, '40', '40', '30', '28', '25', '22', NULL, NULL),
(4, '60', '60', '35', '32', '30', '28', NULL, NULL),
(5, '80', '80', '40', '38', '35', '32', NULL, NULL),
(6, '100', '100', '45', '42', '40', '38', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `heights`
--

CREATE TABLE `heights` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `height` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `heights`
--

INSERT INTO `heights` (`id`, `height`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, NULL),
(2, 2, NULL, NULL),
(3, 3, NULL, NULL),
(4, 4, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `item_weights`
--

CREATE TABLE `item_weights` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `weight` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `item_weights`
--

INSERT INTO `item_weights` (`id`, `weight`, `created_at`, `updated_at`) VALUES
(1, '65', NULL, NULL),
(2, '33', NULL, NULL),
(3, '53', NULL, NULL),
(4, '12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `lift_costs`
--

CREATE TABLE `lift_costs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `floor` int(11) NOT NULL,
  `per_tala_cost` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `lift_costs`
--

INSERT INTO `lift_costs` (`id`, `floor`, `per_tala_cost`, `created_at`, `updated_at`) VALUES
(1, 1, 10, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_07_26_063936_create_admins_table', 1),
(5, '2019_07_29_173558_create_categories_table', 3),
(6, '2019_07_29_173647_create_subcategories_table', 3),
(7, '2019_07_30_051844_create_tags_table', 4),
(12, '2019_08_05_140717_create_names_table', 7),
(13, '2019_08_05_140735_create_ages_table', 7),
(14, '2019_08_05_140805_create_item_weights_table', 7),
(16, '2019_08_05_140910_create_origin_countries_table', 7),
(17, '2019_08_05_140935_create_temperatures_table', 7),
(21, '2019_08_10_193012_create_heights_table', 9),
(22, '2019_08_10_193036_create_widths_table', 9),
(23, '2019_08_05_140649_create_brands_table', 10),
(26, '2019_08_18_114108_create_delivery_costs_table', 12),
(27, '2019_08_19_160531_create_lift_costs_table', 13),
(30, '2019_07_30_054618_create_nurseries_table', 15),
(32, '2019_07_26_063950_create_users_table', 17),
(33, '2018_12_23_120000_create_shoppingcart_table', 18),
(36, '2019_08_19_210359_create_orders_table', 19),
(37, '2019_08_24_155543_create_order_products_table', 19),
(38, '2019_07_30_134618_create_products_table', 20);

-- --------------------------------------------------------

--
-- Table structure for table `names`
--

CREATE TABLE `names` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `names`
--

INSERT INTO `names` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Deshi bot bonsai', NULL, NULL),
(2, 'পাতাবাহার', NULL, NULL),
(3, 'সুগন্ধি পোলাও পাতা চারা', NULL, NULL),
(4, 'Adenium', NULL, NULL),
(5, 'Cactas', NULL, NULL),
(6, 'Orchid Plants', NULL, NULL),
(7, 'টাইম ফুল', NULL, NULL),
(8, 'চায়না কমলা গাছ', NULL, NULL),
(9, 'orange tree', NULL, NULL),
(10, 'এ্যালোভেরা চারা', NULL, NULL),
(11, 'ক্যাকটাস গাছ', NULL, NULL),
(12, 'খেজুর চারা', NULL, NULL),
(13, 'থাই সীডলেস লেবুর চারা', NULL, NULL),
(14, 'ডালিম গাছ', NULL, NULL),
(15, 'hajari golap', NULL, NULL),
(16, 'লটকন গাছ', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `nurseries`
--

CREATE TABLE `nurseries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nurserie_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nurserie_address` text COLLATE utf8mb4_unicode_ci,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_super` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `nurseries`
--

INSERT INTO `nurseries` (`id`, `name`, `phone`, `nurserie_name`, `nurserie_address`, `password`, `is_super`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Abul Kalam Azad', '09623189889', NULL, NULL, '$2y$10$vYaIs1WIwp5AVUHe6ADhQ.SITkqbdpvkT1ai0j..4aZpV1a4cnM2u', 0, NULL, '2019-08-23 14:30:19', '2019-08-23 14:30:19'),
(3, 'Nayem Islam', '01536131168', 'Sudijin', 'Narayanganj', '$2y$10$YMenjiwqIKZ3L7qrHCUPS.LaunPqgB7B7C.cZtb0QmFlW17MUqmuS', 0, NULL, '2019-08-23 15:37:13', '2019-08-23 15:37:13'),
(4, 'Nayem Islam', '1536131168', 'Sudijin', 'Dhaka', '$2y$10$A5uPjxhnJyoVxcnJtus6XO1z9TaFXUXsvgNWgZdZdAgzfdwEqwKsK', 0, NULL, '2019-08-24 12:12:47', '2019-08-24 12:12:47');

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_amount` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `total_product` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `payment` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `address`, `total_amount`, `total_product`, `user_id`, `status`, `payment`, `created_at`, `updated_at`) VALUES
(12, '23940', 'dhaka', '5145', '2', 2, '0', '1', '2019-09-15 12:50:42', '2019-09-15 12:52:58');

-- --------------------------------------------------------

--
-- Table structure for table `order_products`
--

CREATE TABLE `order_products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `order_id` int(11) NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `product_id` int(11) NOT NULL,
  `total_price` int(11) NOT NULL,
  `qty` int(11) NOT NULL,
  `product_cost` int(11) NOT NULL,
  `delivery_cost` int(11) NOT NULL,
  `lift_cost` int(11) NOT NULL,
  `lift` int(11) NOT NULL,
  `nursery_payment` int(191) DEFAULT NULL,
  `our_income` int(191) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `order_products`
--

INSERT INTO `order_products` (`id`, `order_id`, `name`, `product_id`, `total_price`, `qty`, `product_cost`, `delivery_cost`, `lift_cost`, `lift`, `nursery_payment`, `our_income`, `created_at`, `updated_at`) VALUES
(17, 12, 'টাইম ফুল', 1, 5145, 2, 4100, 1025, 20, 2, 3485, 615, '2019-09-15 12:50:42', '2019-09-15 12:52:58');

-- --------------------------------------------------------

--
-- Table structure for table `origin_countries`
--

CREATE TABLE `origin_countries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `country` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `origin_countries`
--

INSERT INTO `origin_countries` (`id`, `country`, `created_at`, `updated_at`) VALUES
(1, 'Bangladesh', NULL, NULL),
(2, 'India', NULL, NULL),
(3, 'Afghanistan', NULL, NULL),
(4, 'Nepal', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `category_id` int(11) NOT NULL,
  `subcategory_id` int(11) NOT NULL,
  `brand_id` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `name_id` int(11) NOT NULL,
  `short_description` text COLLATE utf8mb4_unicode_ci,
  `long_description` text COLLATE utf8mb4_unicode_ci,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `age_id` int(11) NOT NULL,
  `item_weight_id` int(11) NOT NULL,
  `height_id` int(11) NOT NULL,
  `width_id` int(11) NOT NULL,
  `price` int(11) NOT NULL,
  `discount` int(11) NOT NULL,
  `profit_margin` int(11) NOT NULL,
  `sales_price` int(11) NOT NULL,
  `image` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image2` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image3` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `image4` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `video` varchar(500) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `qty` int(11) NOT NULL,
  `with_fruit` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `with_flower` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `origin_country_id` int(11) NOT NULL,
  `temperature_id` int(11) NOT NULL,
  `what_you_will_get` varchar(500) COLLATE utf8mb4_unicode_ci NOT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '0',
  `gift` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `new_arrival` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `nursery_id` int(11) NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `products`
--

INSERT INTO `products` (`id`, `category_id`, `subcategory_id`, `brand_id`, `name_id`, `short_description`, `long_description`, `color`, `age_id`, `item_weight_id`, `height_id`, `width_id`, `price`, `discount`, `profit_margin`, `sales_price`, `image`, `image2`, `image3`, `image4`, `video`, `qty`, `with_fruit`, `with_flower`, `origin_country_id`, `temperature_id`, `what_you_will_get`, `available`, `gift`, `new_arrival`, `nursery_id`, `status`, `created_at`, `updated_at`) VALUES
(1, 5, 12, '2,3,4,5', 7, 'Moszikvuz jog ridububi sirtigame rajga wul tim hodu zacewmi ekfi kassa ale rure pibsecjo orlu. Susev je wubeh se soza epapenoh jersosam ufdo igivuper otuari tev icajon. Roz', 'Wa wanugo udabu iw gel la igtep va kam ojol fin pouno hatiku puf pacosa. Purolif hohgol udopim cowi pa ocucof lajweaco vateh ivepes gefwosij kovne vez kuckeani feh weh colgubadi ponmavrik. Kovgohese cemruv rasew hu ejaji hu wijonzum dogaedu bob ca za seig jiuloot wi hogpa himowe iw uju. Enmeika bucop wu akagunu bev ihuru puf bituvaci nupek run rek ziribteb meroddo. Karifajo ke lifozdev dim', 'Lg', 1, 4, 3, 6, 2000, 250, 15, 2300, '891567885001.jpg', '671567885002.jpg', '941567885002.jpg', '171567885002.jpg', 'Swbi', 130, 'no', 'no', 2, 1, '41', 0, 'no', 'no', 3, 1, '2019-09-07 13:36:43', '2019-09-15 12:50:42'),
(2, 5, 12, '1,3', 16, 'Fo kekweopo logiha giw udaet sihib nim aloit akus kiwbeh sol wilwaslom hivos isuer iguage. Vero ducdilaj veirbid reniga do ko maecwej zagsos dovbo', 'Inme zukuneg ibgug zonjip bup badal ko sukis tipeba wa ujesoihe utalaz niteg oceov. Ab wa rediw hokowwa fodzujej rin linif kaupe rim fehjoz wisnoudu piv gemib luoj. Dourpej kuvri fouho oz vaf zojus podakmu juj nig hol hubo zu salejgo walwogcof gecorbi vomrim. Nidodvi weasa ru da jab zuv baim kohgaffeg gaz iv zalu ligfi gowciwi ceriubi cawe ako eme buhl', 'black', 10, 4, 4, 8, 1376, 26, 10, 1514, '651567895442.jpg', '241567895442.jpeg', '151567895442.jpg', '201567895443.jpeg', 'Mgpamkuau th', 67, 'no', 'no', 4, 4, '72', 0, 'no', 'no', 3, 1, '2019-09-07 16:30:43', '2019-09-07 16:31:15'),
(3, 5, 10, '1,3,5', 16, 'Ta ziago juhekujo li harto beh mam ti soubiosa nasos luscopbo bagoc kunetdir. Haj adewafnid sawig cumaraz edaevudin vinkuv re tirlilup vautiwiz ajopo gaket nef kuubtut egozavok ad lic huunon. Jilsiopu biuvu zeta jibopemu dejnez vi ner bo gipfat iwbitil omu me bekjuk ha ovejo feizzav cogisuw wavpa. Hogdu idunufa zo oza egevsal fumawi ap ijaziagi ep ranidic hermuaga hobudcu locerp', 'Tok godtarudu pal dolokoge mirteve mime colwo fuuge cacuce irmu moharow uwiceghe fido pu wor. Cah jezdef babfodi nubcoz avwekuje gopciw vof mihri ga widu egpoza ti nuamu larawees. Ijies ak erguge ujatuen to ded itikom pehutto apivuk hocledun gi weh afe. Hofki soku pav ij wo ev upom papiw lal mejoc wug ikefu gi ignebam ola', 'black', 10, 4, 4, 8, 467, 26, 10, 514, '531567941958.jpg', '781567941959.jpg', '491567941959.jpg', '721567941960.jpeg', 'Rhh a', 74, 'no', 'no', 4, 4, '50', 0, 'no', 'no', 3, 1, '2019-09-08 05:26:00', '2019-09-15 12:13:16'),
(4, 5, 12, '1,3,5', 16, 'Ziari pezuzab no vebpasod laolu gepwi zirambuv cuba awosetme ha emuto rawar koccinwe erkubwan ulmopod ombenta su efasig. Suval ma guofuha puake nufif dovout is cuki lasvez udmoze kunarti rivrudpo. Umnogi kuov tolupja socpe ursem mir di alasaj owlame ninn', 'Titemir jorne aj bemakiza cangu nuvwouv jujapo gizijivum cipmetgec izbu webadewu fu abiiggis zi otatef recmidec. Sanot wendilzad ciwim keoro hav tol hofla guseru navil zuzdog opu evzonuc le job bam sig toreat. Mu teumran laudoaz cagpo tef ikpu ef look ge mazbug vujnanoz ko pewew azjahgek bisuz. Hifef pi vowiveam ne bo voju', 'Oganaknh gai', 4, 3, 2, 5, 5000, 256, 30, 6500, '451567942005.jpg', '741567942005.jpg', '581567942006.jpg', '231567942006.jpg', 'UaimktaKt ahpu', 489, 'no', 'no', 4, 4, '32', 0, 'no', 'no', 3, 1, '2019-09-08 05:26:46', '2019-09-08 05:27:03');

-- --------------------------------------------------------

--
-- Table structure for table `shoppingcart`
--

CREATE TABLE `shoppingcart` (
  `identifier` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `instance` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `subcategory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `category_id` int(10) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `subcategory`, `category_id`, `created_at`, `updated_at`) VALUES
(9, 'Plants by Type', 5, '2019-08-17 05:22:30', '2019-08-17 05:22:30'),
(10, 'Plants by Location', 5, '2019-08-17 05:22:44', '2019-08-17 05:22:44'),
(11, 'By Features n Uses', 5, '2019-08-17 05:22:56', '2019-08-17 05:22:56'),
(12, 'Plants by Season', 5, '2019-08-17 05:23:10', '2019-08-17 05:23:10'),
(13, 'Flowering Plants', 5, '2019-08-17 05:23:30', '2019-08-17 05:23:30'),
(14, 'Indoor Plants', 5, '2019-08-17 05:23:42', '2019-08-17 05:23:42'),
(15, 'Aa rata aAny', 15, '2019-08-20 13:53:47', '2019-08-20 13:53:47');

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `temperatures`
--

CREATE TABLE `temperatures` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `temperature` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `temperatures`
--

INSERT INTO `temperatures` (`id`, `temperature`, `created_at`, `updated_at`) VALUES
(1, '23', NULL, NULL),
(2, '34', NULL, NULL),
(3, '42', NULL, NULL),
(4, '43', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `phone`, `password`, `status`, `remember_token`, `created_at`, `updated_at`) VALUES
(2, 'Rofequl Islam Nayem', '9623189889', '$2y$10$iq4VS9fSJoaBLm5N5QuUieYtPuASJPDzgziXgstPK4ngjkL6foMrq', 0, 'UuNXpPJp8t6xqIp5b8FLI8pzwqB2zdYA2DUJXb4NXq2LJMoQReM3RiDH31tv', '2019-08-24 12:18:17', '2019-08-24 12:18:17');

-- --------------------------------------------------------

--
-- Table structure for table `widths`
--

CREATE TABLE `widths` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `width` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `widths`
--

INSERT INTO `widths` (`id`, `width`, `created_at`, `updated_at`) VALUES
(3, 1, NULL, NULL),
(4, 2, NULL, NULL),
(5, 3, NULL, NULL),
(6, 4, NULL, NULL),
(7, 5, NULL, NULL),
(8, 6, NULL, NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `admins_email_unique` (`email`);

--
-- Indexes for table `ages`
--
ALTER TABLE `ages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `brands`
--
ALTER TABLE `brands`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `delivery_costs`
--
ALTER TABLE `delivery_costs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `heights`
--
ALTER TABLE `heights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_weights`
--
ALTER TABLE `item_weights`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `lift_costs`
--
ALTER TABLE `lift_costs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `names`
--
ALTER TABLE `names`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `nurseries`
--
ALTER TABLE `nurseries`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `nurseries_phone_unique` (`phone`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_products`
--
ALTER TABLE `order_products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `origin_countries`
--
ALTER TABLE `origin_countries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shoppingcart`
--
ALTER TABLE `shoppingcart`
  ADD PRIMARY KEY (`identifier`,`instance`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `temperatures`
--
ALTER TABLE `temperatures`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_phone_unique` (`phone`);

--
-- Indexes for table `widths`
--
ALTER TABLE `widths`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `ages`
--
ALTER TABLE `ages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `brands`
--
ALTER TABLE `brands`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `delivery_costs`
--
ALTER TABLE `delivery_costs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `heights`
--
ALTER TABLE `heights`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `item_weights`
--
ALTER TABLE `item_weights`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `lift_costs`
--
ALTER TABLE `lift_costs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `names`
--
ALTER TABLE `names`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `nurseries`
--
ALTER TABLE `nurseries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `order_products`
--
ALTER TABLE `order_products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `origin_countries`
--
ALTER TABLE `origin_countries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `temperatures`
--
ALTER TABLE `temperatures`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `widths`
--
ALTER TABLE `widths`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
