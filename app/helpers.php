<?php

use App\brand;
use App\order_product;
use App\product;

function get_brand_by_id($id){
    return brand::find($id);
}

function get_product_by_brand($id){
    return product::whereRaw("find_in_set($id,brand_id)")->where('status',1)->get();
}

function get_product_by_id($id){
    return product::find($id);
}

function get_order_product_by_order_id($id){
    return order_product::where('order_id',$id)->get();
}
