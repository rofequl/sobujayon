<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class order extends Model
{
    public function user(){
        return $this->belongsTo(user::class);
    }

    public function order_product(){
        return $this->hasMany(order_product::class,'order_id');
    }
}
