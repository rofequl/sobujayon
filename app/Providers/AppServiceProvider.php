<?php

namespace App\Providers;

use App\category;
use Illuminate\Support\Facades\Blade;
use View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

        View::composer('*', function($view)
        {
            $view->with('menudata', category::with('subcategory','subcategory.brand')->get());
        });

        Blade::directive('convert', function ($money) {
            return "<?php echo number_format($money); ?>";
        });
    }
}
