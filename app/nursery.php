<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class nursery extends Authenticatable
{
    use Notifiable;

    protected $guard = 'nursery';

    protected $fillable = [
        'name', 'phone','nurserie_name','nurserie_address', 'password',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function product(){
        return $this->hasMany(product::class,'nursery_id');
    }
}
