<?php

namespace App\Http\Controllers;

use App\order;
use App\order_product;
use App\product;
use Illuminate\Http\Request;
use Session;

class OrderController extends Controller
{
    public function AdminUnsoldProduct(){
        $title = 'Product Unsold';
        $order = order::orderBy('id','DESC')->where('status','!=',4)->get();
        return view('admin.sold_product',compact('order','title'));
    }

    public function AdminSoldProduct(){
        $title = 'Product Sold';
        $order = order::orderBy('id','DESC')->where('status',4)->get();
        return view('admin.sold_product',compact('order','title'));
    }

    public function AdminOrderProduct($id){

        $order = order::findOrFail(base64_decode($id));
        return view('admin.order_view',compact('order'));
    }

    public function AdminOrderStatus(Request $request){

        $order = order::findOrFail($request->id);
        $order->status = $request->value;
        $order->save();

        Session::flash('message', 'Status update successfully');
        return redirect()->back();
    }

    public function AdminOrderPaid(Request $request){

        if ($request->approved){
            $order = order::findOrFail(base64_decode($request->approved));
            $order->payment = 1;
            $order->save();

            $order_product = order_product::where('order_id',$order->id)->get();

            foreach ($order_product as $order_products){
                $product = product::findOrFail($order_products->product_id);
                $total_income = ($product->sales_price - $product->discount) * $order_products->qty;
                $our_income = round(($total_income / 100) * $product->profit_margin);
                $nursery_payment = $total_income - $our_income;

                $insert = order_product::findOrFail($order_products->id);
                $insert->nursery_payment = $nursery_payment;
                $insert->our_income = $our_income;
                $insert->save();
            }
        }else{
            $order = order::findOrFail(base64_decode($request->unapproved));
            $order->payment = 0;
            $order->save();
        }

        Session::flash('message', 'Payment update successfully');
        return redirect()->back();
    }

    public function AdminReport(Request $request){
        if ($request->start && $request->end){
            $from = date('Y-m-d',strtotime($request->start));
            $to  = date('Y-m-d',strtotime($request->end));
            $from1 = $request->start;
            $to1 = $request->end;

            $order = order::whereDate('created_at','>=', $from)
                ->whereDate('created_at','<=', $to)->where('payment',1)->get();
            return view('admin.report',compact('order','from1','to1'));
        }
        return view('admin.report');
    }
}
