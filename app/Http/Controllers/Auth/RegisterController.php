<?php
namespace App\Http\Controllers\Auth;
use App\nursery;
use App\user;
use App\admin;
use App\nursary;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Auth;
class RegisterController extends Controller
{

    use RegistersUsers;

    protected $redirectTo = '/';

    public function __construct()
    {
        $this->middleware('guest');
        $this->middleware('guest:admin');
        $this->middleware('guest:user');
        $this->middleware('guest:nursary');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    public function showAdminRegisterForm()
    {
        return view('admin.register');
    }

    protected function createAdmin(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:255|regex:/^[a-zA-Z -]+$/u',
            'email' => 'required|string|email|max:255|unique:admins',
            'password' => 'required|string|min:6',
        ]);
        $admin = admin::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        Auth::guard('admin')->login($admin);

        return redirect()->intended('/admin');
    }

    public function showUserRegisterForm()
    {
        return view('frontend.register');
    }

    protected function createUser(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|regex:/^[a-zA-Z -]+$/u|max:191',
            'phone' => 'required|numeric|digits_between:1,12|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);

        $user = user::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'password' => Hash::make($request->password),
        ]);

        Auth::guard('user')->login($user);

        return redirect()->intended('/');
    }

    public function showNurseryForm()
    {
        return view('nursery.register');
    }

    protected function createNursery(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string|max:191|regex:/^[a-zA-Z -]+$/u',
            'phone' => 'required|numeric|digits_between:1,12|unique:nurseries',
            'nurserie_name' => 'max:191|regex:/^[a-zA-Z -]+$/u',
            'nurserie_address' => 'regex:/^[a-zA-Z -]+$/u',
            'password' => 'required|string|min:6|confirmed',
        ]);
        $admin = nursery::create([
            'name' => $request->name,
            'phone' => $request->phone,
            'nurserie_name' => $request->nurserie_name,
            'nurserie_address' => $request->nurserie_address,
            'password' => Hash::make($request->password),
        ]);

        //Auth::guard('nursery')->login($admin);

        return redirect('admin-nursery')->with('message','Nursery user add successfully');
    }

    protected function create(array $data)
    {
        return user::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

}
