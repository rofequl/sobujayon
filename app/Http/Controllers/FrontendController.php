<?php

namespace App\Http\Controllers;

use App\brand;
use App\delivery_cost;
use App\lift_cost;
use App\order;
use App\order_product;
use App\product;
use Gloudemans\Shoppingcart\Facades\Cart;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Session;
use Jenssegers\Agent\Agent;

class FrontendController extends Controller
{
    public function index()
    {
        $agent = new Agent();
        $brand = brand::where('status', 1)->orderBy('id', 'DESC')->get();

        return view('mobile.index', compact('brand'));
    }

    public function search(Request $request)
    {
        //dd($request->all());
        $product = product::where('status', 1);
        if ($request->category_id != null) {
            $product = $product->where('category_id', $request->category_id);
        }

        if ($request->search != null) {
            $product = $product->whereHas('name', function ($query) use ($request) {
                $query->where('name', 'LIKE', '%' . $request->search . '%');
            });
        }
        $product = $product->paginate(15);
        $brand = false;
        $category_id = $request->category_id;
        $search = $request->search;
        return view('mobile.shop', compact('product', 'brand', 'category_id', 'search'));
    }

    public function shop(Request $request)
    {
        $product = product::where('status', 1)->where('gift', 'no');

        $brand = false;

        if ($request->brand) {
            $brand = brand::find($request->brand);
            if ($brand) {
                $product = $product->whereRaw("find_in_set($request->brand,brand_id)");
            }
        }
        if ($request->type == 'new') {
            $product = $product->orderBy('id', 'Desc');
        }

        if ($request->type == 'high') {
            $product = $product->orderBy(DB::raw("`price` - `discount`"), 'desc');
        }

        if ($request->type == 'low') {
            $product = $product->orderBy(DB::raw("`price` - `discount`"), 'asc');
        }

        $product = $product->paginate(15);
        return view('mobile.shop', compact('product', 'brand'));
    }

    public function gift(Request $request)
    {
        $product = product::where('status', 1)->where('gift', 'yes');

        if ($request->type == 'new') {
            $product = $product->orderBy('id', 'Desc');
        }

        if ($request->type == 'high') {
            $product = $product->orderBy(DB::raw("`price` - `discount`"), 'desc');
        }

        if ($request->type == 'low') {
            $product = $product->orderBy(DB::raw("`price` - `discount`"), 'asc');
        }

        $product = $product->paginate(15);
        return view('mobile.gift', compact('product'));
    }

    public function SingleShop(Request $request)
    {

        if ($request->product) {
            $recent_product = new \Illuminate\Database\Eloquent\Collection;
            if (isset($_COOKIE['recent_product'])) {
                $id = unserialize($_COOKIE['recent_product'], ["allowed_classes" => false]);
                if (!in_array($request->product, $id)) {
                    if (count($id) > 9) {
                        array_shift($id);
                    }
                    array_push($id, $request->product);
                }

                foreach ($id as $single_id) {
                    $all_product = product::where('id', $single_id)->get();
                    if ($all_product->count() > 0) {
                        $recent_product = $recent_product->merge($all_product);
                    }
                }

                setcookie('recent_product', serialize($id), time() + (86400 * 30), "/");
            } else {
                $id = array($request->product);
                setcookie('recent_product', serialize($id), time() + (86400 * 30), "/");
            }
            $product = product::find($request->product);

            return view('mobile.single-shop', compact('product', 'recent_product'));
        } else {
            abort('404');
        }
    }

    public function AddCart(Request $request)
    {


        $qty = $request->qty;
        if ($qty == 'NaN' || $qty == 0) {
            $qty = 1;
        }
        $lift = $request->lift;
        if ($lift == 'NaN' || $lift == 0) {
            $lift = 0;
        }

        $product = product::with('item_weight')->where('id', $request->id)->first();

        $delivery = delivery_cost::where('weight', '>=', $product->item_weight->weight)->orderBy('weight')->first();

        if ($qty <= 5) {
            $delivery_percent = $delivery->below_5;
        } elseif ($qty > 5 && $qty <= 10) {
            $delivery_percent = $delivery->below_10;
        } elseif ($qty > 10 && $qty <= 20) {
            $delivery_percent = $delivery->below_20;
        } elseif ($qty > 20) {
            $delivery_percent = $delivery->up_20;
        } else {
            $delivery_percent = $delivery->below_5;
        }

        $product_cost = ($product->sales_price - $product->discount) * $qty;

        $delivery_cost = round(($product_cost / 100) * $delivery_percent);

        $lift_cost = lift_cost::first();

        $lift_cost = $lift_cost->per_tala_cost * $lift;

        $total_cost = $product_cost + $delivery_cost + $lift_cost;

        $cart = Cart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id == $request->id;
        });

        if ($cart->isNotEmpty()) {
            return 1;
        } else {
            Cart::add($product->id, $product->name->name, $qty, $total_cost, $product->item_weight_id, ['product_cost' => $product_cost,
                'delivery_cost' => $delivery_cost, 'lift_cost' => $lift_cost, 'lift' => $lift]);

            $output = array(
                'action' => 3,
                'qty' => Cart::content()->count()
            );
            return $output;
        }


    }

    public function TotalCost(Request $request)
    {
        $qty = $request->qty;
        if ($qty == 'NaN' || $qty == 0) {
            $qty = 1;
        }
        $lift = $request->lift;
        if ($lift == 'NaN' || $lift == 0) {
            $lift = 0;
        }

        $product = product::with('item_weight')->where('id', $request->id)->first();

        $delivery = delivery_cost::where('weight', '>=', $product->item_weight->weight)->orderBy('weight')->first();

        if ($delivery) {
            if ($qty <= 5) {
                $delivery_percent = $delivery->below_5;
            } elseif ($qty > 5 && $qty <= 10) {
                $delivery_percent = $delivery->below_10;
            } elseif ($qty > 10 && $qty <= 20) {
                $delivery_percent = $delivery->below_20;
            } elseif ($qty > 20) {
                $delivery_percent = $delivery->up_20;
            } else {
                $delivery_percent = $delivery->below_5;
            }
        } else {
            $delivery_percent = 0;
        }


        $product_cost = ($product->sales_price - $product->discount) * $qty;

        $delivery_cost = round(($product_cost / 100) * $delivery_percent);

        $lift_cost = lift_cost::first();

        $lift_cost = $lift_cost->per_tala_cost * $lift;

        $total_cost = $product_cost + $delivery_cost + $lift_cost;

        $output = array(
            'product_cost' => $product_cost,
            'delivery_cost' => $delivery_cost,
            'lift_cost' => $lift_cost,
            'total_cost' => $total_cost,
        );
        echo json_encode($output);
    }

    public function ShopCart()
    {
        //dd(Cart::content());
        return view('mobile.cart');
    }

    public function ShopCartRemove(Request $request)
    {
        if ($request->remove) {
            Cart::remove($request->remove);
            return redirect()->back();
        } else {
            abort('404');
        }
    }

    public function checkout()
    {
        if (Session::get('link') != null) {
            Session::forget('link');
        }
        if (!Auth::guard('user')->check()) {
            Session::put('link', 'checkout');
            return redirect('login-user');
        }


        return view('mobile.checkout');
    }

    public function CheckoutStore(Request $request)
    {
        if (Cart::content()->count() == 0) {
            return redirect('shop-cart');
        }

        $request->validate([
            'address' => 'required',
        ]);


        $order_id = Auth::guard('user')->user()->id . rand(100, 999) . order::get()->count();
        $total_amount = 0;
        foreach (Cart::content() as $cart) {
            $total_amount = $cart->price + $total_amount;
        }
        $order = new order();
        $order->order_id = $order_id;
        $order->address = $request->address;
        $order->total_amount = $total_amount;
        $order->total_product = Cart::count();
        $order->user_id = Auth::guard('user')->user()->id;
        $order->save();

        foreach (Cart::content() as $cart) {
            $insert = new order_product();
            $insert->order_id = $order->id;
            $insert->name = $cart->name;
            $insert->product_id = $cart->id;
            $insert->total_price = $cart->price;
            $insert->qty = $cart->qty;
            $insert->product_cost = $cart->options->product_cost;
            $insert->delivery_cost = $cart->options->delivery_cost;
            $insert->lift_cost = $cart->options->lift_cost;
            $insert->lift = $cart->options->lift;
            $insert->save();

            $product = product::find($cart->id);
            $product_qty = $product->qty - $cart->qty;
            $product->qty = $product_qty;
            $product->save();
        }

        Cart::destroy();

        Session::flash('message', 'Order successfully');
        return redirect('order-list');
    }

    public function OrderList()
    {

        $order = order::where('user_id', Auth::guard('user')->user()->id)->orderBy('id', 'DESC')->paginate(5);
        return view('mobile.order_list', compact('order'));

    }

    public function OrderListCancel($id)
    {
        $order = order_product::find($id);

        $product = product::find($order->product_id);
        $qty = $product->qty + $order->qty;
        $product->qty = $qty;
        $product->save();

        $order_table = order::find($order->order_id);
        $qty = $order_table->total_product - $order->qty;
        $price = $order_table->total_amount - $order->total_price;
        if ($qty == 0) {
            $order_table->delete();
        } else {
            $order_table->total_product = $qty;
            $order_table->total_amount = $price;
            $order_table->save();
        }

        $order->delete();

        Session::flash('message', 'Order Cancel');
        return redirect('order-list');
    }

    public function tracking(Request $request)
    {
        $order = false;
        $order_id = false;
        if ($request->track) {
            $order_id = $request->track;
            $order = order::where('order_id', $request->track)->orderBy('id', 'DESC')->get();
            if (!$order->count() > 0) {
                $order = false;
                Session::flash('message', 'No result found');
            }
        }
        return view('mobile.tracking', compact('order', 'order_id'));
    }

    public function CartUpdate(Request $request)
    {
        $rowId = $request->rowId;
        $qty = $request->qty;

        if (!Cart::get($rowId)) {
            Session::flash('message', 'No result found');
            return redirect()->back();
        }

        $cart = Cart::get($rowId);
        $product = product::findOrFail($cart->id);
        if ($qty < 1) {
            Session::flash('message', 'Empty product, atleast only one add');
            return redirect()->back();
        } elseif ($qty > $product->qty) {
            Session::flash('message', 'Sorry only ' . $product->qty . ' product available.');
            return redirect()->back();
        }

        $delivery = delivery_cost::where('weight', '>=', $product->item_weight->weight)->orderBy('weight')->first();

        if ($qty <= 5) {
            $delivery_percent = $delivery->below_5;
        } elseif ($qty > 5 && $qty <= 10) {
            $delivery_percent = $delivery->below_10;
        } elseif ($qty > 10 && $qty <= 20) {
            $delivery_percent = $delivery->below_20;
        } elseif ($qty > 20) {
            $delivery_percent = $delivery->up_20;
        } else {
            $delivery_percent = $delivery->below_5;
        }

        $product_cost = ($product->sales_price - $product->discount) * $qty;

        $delivery_cost = round(($product_cost / 100) * $delivery_percent);

        $total_cost = $product_cost + $delivery_cost + $cart->options->lift_cost;

        Cart::update($rowId, ['qty' => $qty, 'price' => $total_cost, 'options' => ['delivery_cost' => $delivery_cost
            , 'lift_cost' => $cart->options->lift_cost, 'lift' => $cart->options->lift, 'product_cost' => $cart->options->product_cost]]);

        Session::flash('message', 'Product Qty update successfully');
        return redirect()->back();

    }
}
