<?php

namespace App\Http\Controllers;

use App\age;
use App\brand;
use App\category;
use App\height;
use App\item_weight;
use App\name;
use App\nursery;
use App\order_product;
use App\origin_country;
use App\product;
use App\shipping_weight;
use App\subcategory;
use App\temperature;
use App\width;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Image;

use Session;

class NurseryController extends Controller
{
    public function AdminNursery()
    {
        return view('admin.nursery.nursery_add');
    }

    public function AdminNurseryList()
    {
        $nursery = nursery::select('phone','nurserie_name','nurserie_address', 'name', 'id')->orderBy('id', 'DESC')->get();
        return view('admin.nursery.nurcery_list', compact('nursery'));
    }

    public function AdminNurseryListDelete(Request $request)
    {
        if ($request->delete) {
            $product = product::where('nursery_id',base64_decode($request->delete))->get();
            if ($product->count() > 0){
                Session::flash('message', 'Nursery add product, not deleted');
                return redirect('admin-nursery-list');
            }
            $category = nursery::find(base64_decode($request->delete));
            $category->delete();
            Session::flash('message', 'Nursery Delete Successfully');
            return redirect('admin-nursery-list');
        } else {
            echo "Something Wrong";
        }
    }

    public function NurseryProduct()
    {
        $category = category::all();
        $subcategory = subcategory::all();
        $name = name::all();
        $age = age::all();
        $brand = brand::all();
        $item_weight = item_weight::all();
        $origin_country = origin_country::all();
        $width = width::all();
        $height = height::all();
        $temperature = temperature::all();
        return view('nursery.product', compact('category', 'subcategory',
            'name', 'age', 'item_weight', 'origin_country', 'width', 'height', 'temperature','brand'));
    }

    public function NurseryProductList()
    {
        $product = product::with('category', 'name')->where('nursery_id',Auth::guard('nursery')->user()->id)->orderBy('id', 'DESC')->get();
        return view('nursery.product_list', compact('product'));
    }

    public function NurseryProductAdd(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'name_id' => 'required',
            'color' => 'max:191',
            'age_id' => 'required|digits_between:1,10|integer',
            'item_weight_id' => 'required|digits_between:1,10|integer',
            'height_id' => 'required|digits_between:1,10|integer',
            'width_id' => 'required|digits_between:1,10|integer',
            'price' => 'required|digits_between:1,10|integer',
            'discount' => 'required|digits_between:1,10|integer',
            'profit_margin' => 'required|digits_between:1,10|integer',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1000',
            'image2' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1000',
            'image3' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1000',
            'image4' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:1000',
            'video' => 'max:191|regex:/^[a-zA-Z0-9 -.,!?]+$/u',
            'qty' => 'required|digits_between:1,10|integer',
            'origin_country_id' => 'required',
            'temperature_id' => 'required|digits_between:1,10|integer',
            'what_you_will_get' => 'required|max:500|regex:/^[a-zA-Z0-9 -.,!?]+$/u',
            'gift' => 'required',
            'new_arrival' => 'required',
        ]);

        //dd($request->all());

        $insert = new product();
        if ($request->gift == 'no'){
            $request->validate([
                'brand_id' => 'required',
            ]);
            $insert->brand_id = implode(",",$request->brand_id);
        }
        $insert->category_id = $request->category_id;
        $insert->subcategory_id = $request->subcategory_id;
        $insert->name_id = $request->name_id;
        $insert->short_description = $request->short_description;
        $insert->long_description = $request->long_description;
        $insert->color = $request->color;
        $insert->age_id = $request->age_id;
        $insert->item_weight_id = $request->item_weight_id;
        $insert->height_id = $request->height_id;
        $insert->width_id = $request->width_id;
        $insert->price = $request->price;
        $insert->discount = $request->discount;
        $insert->profit_margin = $request->profit_margin;
        $insert->sales_price = $request->price + round(($request->price/100)* $request->profit_margin);
        $insert->video = $request->video;
        $insert->qty = $request->qty;
        $insert->with_fruit = $request->with_fruit;
        $insert->with_flower = $request->with_flower;
        $insert->origin_country_id = $request->origin_country_id;
        $insert->temperature_id = $request->temperature_id;
        $insert->what_you_will_get = $request->what_you_will_get;
        $insert->gift = $request->gift;
        $insert->new_arrival = $request->new_arrival;
        $insert->nursery_id = Auth::guard('nursery')->user()->id;

        if ($request->hasFile('image')) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $name = rand(10, 100) . time() . "." . $extension;
            $file = $request->file('image');
            $img = Image::make($file);
            $img1 = $img->resize(1126, 1126)->encode();
            Storage::put($name, $img1);
            Storage::move($name, 'public/tree/' . $name);
            $img2 = $img->resize(500, 500)->encode();
            Storage::put($name, $img2);
            Storage::move($name, 'public/tree/convert/' . $name);
            $insert->image = $name;
        }

        if ($request->hasFile('image2')) {
            $extension1 = $request->file('image2')->getClientOriginalExtension();
            $fileStore1 = rand(10, 100) . time() . "." . $extension1;
            $file = $request->file('image2');
            $img = Image::make($file)->resize(1126, 1126)->encode();
            Storage::put($fileStore1, $img);
            Storage::move($fileStore1, 'public/tree/' . $fileStore1);
            $insert->image2 = $fileStore1;
        }

        if ($request->hasFile('image3')) {
            $extension2 = $request->file('image3')->getClientOriginalExtension();
            $fileStore2 = rand(10, 100) . time() . "." . $extension2;
            $file = $request->file('image3');
            $img = Image::make($file)->resize(1126, 1126)->encode();
            Storage::put($fileStore2, $img);
            Storage::move($fileStore2, 'public/tree/' . $fileStore2);
            $insert->image3 = $fileStore2;
        }

        if ($request->hasFile('image4')) {
            $extension3 = $request->file('image4')->getClientOriginalExtension();
            $fileStore3 = rand(10, 100) . time() . "." . $extension3;
            $file = $request->file('image4');
            $img = Image::make($file)->resize(1126, 1126)->encode();
            Storage::put($fileStore3, $img);
            Storage::move($fileStore3, 'public/tree/' . $fileStore3);
            $insert->image4 = $fileStore3;
        }
        $insert->save();

        Session::flash('message', 'Product add successfully');
        return redirect('nursery-product');

    }

    public function NurseryProductUpdate(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'subcategory_id' => 'required',
            'name_id' => 'required',
            'color' => 'max:191',
            'age_id' => 'required|digits_between:1,10|integer',
            'item_weight_id' => 'required|digits_between:1,10|integer',
            'height_id' => 'required|digits_between:1,10|integer',
            'width_id' => 'required|digits_between:1,10|integer',
            'price' => 'required|digits_between:1,10|integer',
            'discount' => 'required|digits_between:1,10|integer',
            'profit_margin' => 'required|digits_between:1,10|integer',
            'image' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'image2' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'image3' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'image4' => 'image|mimes:jpeg,png,jpg,gif,svg',
            'video' => 'max:191',
            'qty' => 'required|digits_between:1,10|integer',
            'origin_country_id' => 'required',
            'temperature_id' => 'required|digits_between:1,10|integer',
            'what_you_will_get' => 'required|max:500',
            'gift' => 'required',
            'new_arrival' => 'required',
        ]);


        $insert = product::find($request->id);
        if ($request->gift == 'no'){
            $request->validate([
                'brand_id' => 'required',
            ]);
            $insert->brand_id = implode(",",$request->brand_id);
        }
        $insert->category_id = $request->category_id;
        $insert->subcategory_id = $request->subcategory_id;
        $insert->name_id = $request->name_id;
        $insert->short_description = $request->short_description;
        $insert->long_description = $request->long_description;
        $insert->color = $request->color;
        $insert->age_id = $request->age_id;
        $insert->item_weight_id = $request->item_weight_id;
        $insert->height_id = $request->height_id;
        $insert->width_id = $request->width_id;
        $insert->price = $request->price;
        $insert->discount = $request->discount;
        $insert->profit_margin = $request->profit_margin;
        $insert->video = $request->video;
        $insert->qty = $request->qty;
        $insert->with_fruit = $request->with_fruit;
        $insert->with_flower = $request->with_flower;
        $insert->origin_country_id = $request->origin_country_id;
        $insert->temperature_id = $request->temperature_id;
        $insert->what_you_will_get = $request->what_you_will_get;
        $insert->gift = $request->gift;
        $insert->new_arrival = $request->new_arrival;

        if ($request->hasFile('image')) {
            $extension = $request->file('image')->getClientOriginalExtension();
            $name = rand(10, 100) . time() . "." . $extension;
            $file = $request->file('image');
            $img = Image::make($file);
            $img1 = $img->resize(1126, 1126)->encode();
            Storage::put($name, $img1);
            Storage::move($name, 'public/tree/' . $name);
            $img2 = $img->resize(500, 500)->encode();
            Storage::put($name, $img2);
            Storage::move($name, 'public/tree/convert/' . $name);
            $insert->image = $name;
        }

        if ($request->hasFile('image2')) {
            $extension1 = $request->file('image2')->getClientOriginalExtension();
            $fileStore1 = rand(10, 100) . time() . "." . $extension1;
            $file = $request->file('image2');
            $img = Image::make($file)->resize(1126, 1126)->encode();
            Storage::put($fileStore1, $img);
            Storage::move($fileStore1, 'public/tree/' . $fileStore1);
            $insert->image2 = $fileStore1;
        }

        if ($request->hasFile('image3')) {
            $extension2 = $request->file('image3')->getClientOriginalExtension();
            $fileStore2 = rand(10, 100) . time() . "." . $extension2;
            $file = $request->file('image3');
            $img = Image::make($file)->resize(1126, 1126)->encode();
            Storage::put($fileStore2, $img);
            Storage::move($fileStore2, 'public/tree/' . $fileStore2);
            $insert->image3 = $fileStore2;
        }

        if ($request->hasFile('image4')) {
            $extension3 = $request->file('image4')->getClientOriginalExtension();
            $fileStore3 = rand(10, 100) . time() . "." . $extension3;
            $file = $request->file('image4');
            $img = Image::make($file)->resize(1126, 1126)->encode();
            Storage::put($fileStore3, $img);
            Storage::move($fileStore3, 'public/tree/' . $fileStore3);
            $insert->image4 = $fileStore3;
        }
        $insert->save();

        Session::flash('message', 'Product update successfully');
        return redirect('admin-unapproved-product-single/'.base64_encode($request->id));

    }

    public function SelectSubcategory(Request $request)
    {
        return json_encode(subcategory::where('category_id', $request->id)->get());
    }

    public function SelectBrand(Request $request)
    {
        return json_encode(brand::where('subcategory_id', $request->id)->get());
    }

    public function NurseryUnapprovedProduct()
    {
        $product = product::where('status', 0)->where('nursery_id',Auth::guard('nursery')->user()->id)->orderBy('id','DESC')->get();
        return view('nursery.product.unapproved_product', compact('product'));
    }

    public function NurseryApprovedProduct()
    {
        $product = product::where('status', 1)->where('nursery_id',Auth::guard('nursery')->user()->id)->orderBy('id','DESC')->get();
        return view('nursery.product.approve_product', compact('product'));
    }

    public function NurserySoldProduct()
    {
        $product = order_product::whereHas('product', function ($query) {
            $query->where('nursery_id',Auth::guard('nursery')->user()->id);
        })->where('nursery_payment','!=',NULL)->orderBy('id','DESC')->get();
        return view('nursery.product.sold_product', compact('product'));
    }

    public function NurseryOrderProduct()
    {
        $product = order_product::whereHas('product', function ($query) {
            $query->where('nursery_id',Auth::guard('nursery')->user()->id);
        })->where('nursery_payment','=',NULL)->orderBy('id','DESC')->get();
        return view('nursery.product.order_product', compact('product'));
    }

}
